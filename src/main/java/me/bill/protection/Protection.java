package me.bill.protection;

import me.bill.protection.command.defaults.CommandConfig;
import me.bill.protection.command.defaults.CommandFlag;
import me.bill.protection.command.defaults.CommandRegion;
import me.bill.protection.command.defaults.CommandSelection;
import me.bill.protection.configuration.Configuration;
import me.bill.protection.database.Database;
import me.bill.protection.database.MongoDB;
import me.bill.protection.database.MySQL;
import me.bill.protection.listeners.CustomEventsListener;
import me.bill.protection.listeners.PlayerListener;
import me.bill.protection.listeners.RegionListener;
import me.bill.protection.player.PlayerManager;
import me.bill.protection.region.RegionManager;
import me.bill.protection.synchronization.Async;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Protection extends JavaPlugin {

    private static Protection instance;

    public static Protection getInstance() {
        return instance;
    }

    private Database database;
    private RegionManager regionManager;
    private Configuration configuration;
    private PlayerManager playerManager;

    public Database getDataBase() {
        return database;
    }

    public RegionManager getRegionManager() {
        return regionManager;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public PlayerManager getPlayerManager() {
        return playerManager;
    }

    @Override
    public void onEnable() {
        instance = this;

        saveDefaultConfig();

        if (!loadDatabase()) {
            getServer().getConsoleSender().sendMessage(ChatColor.RED + "Unsupported database type: " + getConfig().getString("database.type") + ", disabling plugin");
            setEnabled(false);
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        regionManager = new RegionManager();
        configuration = new Configuration(getConfig());
        playerManager = new PlayerManager();


        registerListeners();
        registerCommands();

        for (Player online : Bukkit.getOnlinePlayers()) {
            playerManager.loadData(online.getUniqueId());
        }
    }

    @Override
    public void onDisable() {
        instance = null;

        if (database != null) {
            database.unload();
            database = null;
        }

        if (regionManager != null) {
            regionManager.unload();
            regionManager = null;
        }

        if (playerManager != null) {
            playerManager.unload();
            playerManager = null;
        }

        Async.unload();
    }

    private boolean loadDatabase() {
        String host = getConfig().getString("database.host", "127.0.0.1");
        int port = getConfig().getInt("database.port", 3306);
        String userName = getConfig().getString("database.username", "username");
        String password = getConfig().getString("database.password", "password");
        String database = getConfig().getString("database.database", "protection");
        switch (getConfig().getString("database.type", "MySQL").toLowerCase()) {
            case "mongo":
            case "mongodb":
                this.database = new MongoDB(host, port, userName, password, getConfig().getString("database.MongoDB.authdb", "admin"), database);
                return true;
            case "sql":
            case "mysql":
                this.database = new MySQL(host, port, userName, password, database);
                return true;
            default:
                return false;
        }
    }

    private void registerListeners() {
        PluginManager pm = getServer().getPluginManager();

        pm.registerEvents(new PlayerListener(), this);
        pm.registerEvents(new RegionListener(), this);
        pm.registerEvents(new CustomEventsListener(), this);
    }

    private void registerCommands() {
        new CommandRegion();
        new CommandSelection();
        new CommandFlag();
        new CommandConfig();
    }

    @Override
    public void reloadConfig() {
        super.reloadConfig();
        configuration = new Configuration(getConfig());
    }
}
