package me.bill.protection.chat;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class Chat {

    public static void sendMessage(CommandSender sender, ChatType chatType, String message) {
        sender.sendMessage(chatType.getPrefix() + message.replace("{%m}", chatType.getMarkupColor()).replace("{%n}", chatType.getDefaultColor()));
    }

    public static void commandUsage(CommandSender sender, String commandUsage, String explanation) {
        sendMessage(sender, ChatType.ERROR, "Correct usage: {%m}/" + commandUsage + " {%n}- " + explanation);
    }

    public static void noPerm(CommandSender sender, String permission) {
        sendMessage(sender, ChatType.ERROR, "You don't have the permissions to perform this command! ({%m}" + permission + "{%n})");
    }

    public static void commandHelp(CommandSender sender, String command, String explanation) {
        sendMessage(sender, ChatType.HELP, "{%m}/" + command + " {%n}- " + ChatColor.WHITE + explanation);
    }

}
