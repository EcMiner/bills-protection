package me.bill.protection.chat;

import org.bukkit.ChatColor;

public enum ChatType {

    ERROR(ChatColor.DARK_RED + "\u00BB " + ChatColor.GRAY, ChatColor.RED, ChatColor.GRAY),
    SUCCESS(ChatColor.DARK_GREEN + "\u00BB " + ChatColor.GRAY, ChatColor.GREEN, ChatColor.GRAY),
    HELP(ChatColor.AQUA + "\u00BB " + ChatColor.GRAY, ChatColor.AQUA, ChatColor.GRAY);

    private final String prefix;
    private final String markupColor;
    private final String defaultColor;

    ChatType(String prefix, ChatColor markup, ChatColor defaultColor) {
        this(prefix, markup.toString(), defaultColor.toString());
    }

    ChatType(String prefix, String markupColor, String defaultColor) {
        this.prefix = prefix;
        this.markupColor = markupColor;
        this.defaultColor = defaultColor;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getMarkupColor() {
        return markupColor;
    }

    public String getDefaultColor() {
        return defaultColor;
    }
}
