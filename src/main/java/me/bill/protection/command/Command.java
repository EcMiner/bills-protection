package me.bill.protection.command;

import me.bill.protection.Protection;
import me.bill.protection.chat.Chat;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public abstract class Command implements CommandExecutor {

    private final String command;
    private String permissionNode;

    public Command(String command) {
        this.command = command;
        Protection.getInstance().getCommand(command).setExecutor(this);
    }

    public Command(String command, String permissionNode) {
        this(command);
        this.permissionNode = permissionNode;
    }

    @Override
    public final boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] strings) {
        if (permissionNode != null && !commandSender.hasPermission(permissionNode)) {
            Chat.noPerm(commandSender, permissionNode);
            return false;
        }

        this.onCommandExecuted(commandSender, s, strings);
        return true;
    }

    public abstract void onCommandExecuted(CommandSender sender, String label, String[] args);

    public String getCommand() {
        return command;
    }

    public String getPermissionNode() {
        return permissionNode;
    }

    public boolean isInt(String value) {
        try {
            Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }
}
