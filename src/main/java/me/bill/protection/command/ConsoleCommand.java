package me.bill.protection.command;

import me.bill.protection.chat.Chat;
import me.bill.protection.chat.ChatType;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

public abstract class ConsoleCommand extends Command {

    public ConsoleCommand(String command) {
        super(command);
    }

    public ConsoleCommand(String command, String permissionNode) {
        super(command, permissionNode);
    }

    @Override
    public final void onCommandExecuted(CommandSender sender, String label, String[] args) {
        if (sender instanceof ConsoleCommandSender) {
            onConsoleExecuted((ConsoleCommandSender) sender, label, args);
        } else {
            Chat.sendMessage(sender, ChatType.ERROR, "Only the console can perform this command!");
        }
    }

    public abstract void onConsoleExecuted(ConsoleCommandSender sender, String label, String[] args);
}
