package me.bill.protection.command;

import me.bill.protection.chat.Chat;
import me.bill.protection.chat.ChatType;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class PlayerCommand extends Command {


    public PlayerCommand(String command) {
        super(command);
    }

    public PlayerCommand(String command, String permissionNode) {
        super(command, permissionNode);
    }

    @Override
    public final void onCommandExecuted(CommandSender sender, String label, String[] args) {
        if (!(sender instanceof Player)) {
            Chat.sendMessage(sender, ChatType.ERROR, "Only an in-game player can perform this command!");
            return;
        }
        Player player = (Player) sender;
        onPlayerExecuted(player, label, args);
    }

    public abstract void onPlayerExecuted(Player player, String label, String[] args);
}
