package me.bill.protection.command.defaults;

import me.bill.protection.Protection;
import me.bill.protection.chat.Chat;
import me.bill.protection.chat.ChatType;
import me.bill.protection.command.Command;
import me.bill.protection.configuration.Configuration;
import me.bill.protection.permission.Permissions;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;

public class CommandConfig extends Command {

    public CommandConfig() {
        super("config");
    }

    @Override
    public void onCommandExecuted(CommandSender sender, String label, String[] args) {
        if (args.length > 0) {
            Configuration config = Protection.getInstance().getConfiguration();
            switch (args[0].toLowerCase()) {
                case "reload":
                    if (sender.hasPermission(Permissions.configReload)) {
                        if (args.length == 1) {
                            Protection.getInstance().reloadConfig();
                            Chat.sendMessage(sender, ChatType.SUCCESS, "The config has been reloaded!");
                        } else {
                            Chat.commandUsage(sender, "config reload", "Reloads the configuration and updates any changes made in it.");
                        }
                    } else {
                        Chat.noPerm(sender, Permissions.configReload);
                    }
                    break;
                case "tool":
                    if (sender.hasPermission(Permissions.configToolView)) {
                        if (args.length == 1) {
                            Chat.sendMessage(sender, ChatType.SUCCESS, "Current global selection tool: {%m}" + config.getSelectionTool().name().toLowerCase().replace("_", " ") + " (" + config.getSelectionTool().getId() + ")");
                        } else {
                            Chat.commandUsage(sender, "config tool", "Shows what the global selection tool currently is");
                        }
                    } else {
                        Chat.noPerm(sender, Permissions.configToolView);
                    }
                    break;
                case "settool":
                    if (sender.hasPermission(Permissions.configToolSet)) {
                        if (args.length == 2) {
                            if (isInt(args[1])) {
                                Material tool = Material.getMaterial(Integer.parseInt(args[1]));
                                if (tool != null) {
                                    config.setSelectionTool(tool);
                                    Chat.sendMessage(sender, ChatType.SUCCESS, "You set the global selection tool to {%m}" + tool.name().toLowerCase().replace("_", " ") + " (" + tool.getId() + ")");
                                } else {
                                    Chat.sendMessage(sender, ChatType.ERROR, "No item by the id {%m}" + args[1] + "{%n} exists!");
                                }
                            } else {
                                Chat.sendMessage(sender, ChatType.ERROR, "The item id must be a number! (given {%m}" + args[1] + "{%n})");
                            }
                        } else {
                            Chat.commandUsage(sender, "config settool", "Sets the global selection tool");
                        }
                    } else {
                        Chat.noPerm(sender, Permissions.configToolSet);
                    }
                    break;
                case "adminscanban":
                    if (sender.hasPermission(Permissions.configAdminsCanBanView)) {
                        if (args.length == 1) {
                            Chat.sendMessage(sender, config.adminsCanBan() ? ChatType.SUCCESS : ChatType.ERROR, "Admins can ban: {%m}" + config.adminsCanBan());
                        } else {
                            Chat.commandUsage(sender, "config adminscanban", "Shows you whether region admins can ban other players");
                        }
                    } else {
                        Chat.noPerm(sender, Permissions.configAdminsCanBanView);
                    }
                    break;
                case "setadminscanban":
                    if (sender.hasPermission(Permissions.configAdminsCanBanSet)) {
                        if (args.length == 2 && (args[1].equalsIgnoreCase("true") || args[1].equalsIgnoreCase("false"))) {
                            config.setAdminsCanBan(args[1].equalsIgnoreCase("true"));
                            Chat.sendMessage(sender, config.adminsCanBan() ? ChatType.SUCCESS : ChatType.ERROR, "You set changed admins can ban to: {%m}" + config.adminsCanBan());
                        } else {
                            Chat.commandUsage(sender, "config setadminscanban <true / false>", "Sets whether region admins can ban other players");
                        }
                    } else {
                        Chat.noPerm(sender, Permissions.configAdminsCanBanSet);
                    }
                    break;
                case "adminscanunban":
                    if (sender.hasPermission(Permissions.configAdminsCanUnbanView)) {
                        if (args.length == 1) {
                            Chat.sendMessage(sender, config.adminsCanUnban() ? ChatType.SUCCESS : ChatType.ERROR, "Admins can un-ban: {%m}" + config.adminsCanUnban());
                        } else {
                            Chat.commandUsage(sender, "config adminscanban", "Shows you whether region admins can un-ban players");
                        }
                    } else {
                        Chat.noPerm(sender, Permissions.configAdminsCanUnbanView);
                    }
                    break;
                case "setadminscanunban":
                    if (sender.hasPermission(Permissions.configAdminsCanUnbanSet)) {
                        if (args.length == 2 && (args[1].equalsIgnoreCase("true") || args[1].equalsIgnoreCase("false"))) {
                            config.setAdminsCanUnban(args[1].equalsIgnoreCase("true"));
                            Chat.sendMessage(sender, config.adminsCanBan() ? ChatType.SUCCESS : ChatType.ERROR, "You set changed admins can un-ban to: {%m}" + config.adminsCanUnban());
                        } else {
                            Chat.commandUsage(sender, "config setadminscanunban <true / false>", "Sets whether region admins can un-ban players");
                        }
                    } else {
                        Chat.noPerm(sender, Permissions.configAdminsCanUnbanSet);
                    }
                    break;
                case "help":
                default:
                    sendHelp(sender);
                    break;
            }
        } else {
            sendHelp(sender);
        }
    }

    private void sendHelp(CommandSender sender) {
        if (sender.hasPermission(Permissions.configHelp)) {
            Chat.sendMessage(sender, ChatType.HELP, "Showing help for the command {%m}/config{%n}:");
            Chat.commandHelp(sender, "config help", "Shows the help for all the sub commands of the /config command");
            if (sender.hasPermission(Permissions.configReload)) {
                Chat.commandHelp(sender, "config reload", "Reloads the configuration and updates any changes made in it");
            }
            if (sender.hasPermission(Permissions.configToolView)) {
                Chat.commandHelp(sender, "config tool", "Shows what the global selection tool currently is");
            }
            if (sender.hasPermission(Permissions.configToolSet)) {
                Chat.commandHelp(sender, "config settool", "Sets the global selection tool");
            }
            if (sender.hasPermission(Permissions.configAdminsCanBanView)) {
                Chat.commandHelp(sender, "config adminscanban", "Shows you whether region admins can ban other players");
            }
            if (sender.hasPermission(Permissions.configAdminsCanBanSet)) {
                Chat.commandHelp(sender, "config setadminscanban <true / false>", "Sets whether region admins can ban other players");
            }
            if (sender.hasPermission(Permissions.configAdminsCanUnbanView)) {
                Chat.commandHelp(sender, "config adminscanban", "Shows you whether region admins can un-ban players");
            }
            if (sender.hasPermission(Permissions.configAdminsCanUnbanSet)) {
                Chat.commandHelp(sender, "config setadminscanunban <true / false>", "Sets whether region admins can un-ban players");
            }
        } else {
            Chat.noPerm(sender, Permissions.configHelp);
        }
    }
}
