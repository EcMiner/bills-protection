package me.bill.protection.command.defaults;

import me.bill.protection.chat.Chat;
import me.bill.protection.chat.ChatType;
import me.bill.protection.command.Command;
import me.bill.protection.permission.Permissions;
import me.bill.protection.region.Flag;
import org.apache.commons.lang.StringUtils;
import org.bukkit.command.CommandSender;

public class CommandFlag extends Command {

    public CommandFlag() {
        super("flag", Permissions.flagsHelp);
    }

    @Override
    public void onCommandExecuted(CommandSender sender, String label, String[] args) {
        if (args.length == 1) {
            Flag flag = Flag.fromName(args[0]);
            if (flag != null) {
                Chat.sendMessage(sender, ChatType.SUCCESS, "{%m}Description for " + flag.name().toLowerCase() + "{%n}: " + flag.getDescription());
                return;
            }
        }
        Chat.commandHelp(sender, "flag <flag name>", "Shows the description of a specific flag");
        Chat.sendMessage(sender, ChatType.HELP, "Available flags: {%m}" + StringUtils.join(Flag.values(), ", ").toLowerCase());
    }
}
