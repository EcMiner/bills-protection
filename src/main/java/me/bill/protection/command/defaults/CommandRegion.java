package me.bill.protection.command.defaults;

import me.bill.protection.Protection;
import me.bill.protection.chat.Chat;
import me.bill.protection.chat.ChatType;
import me.bill.protection.command.PlayerCommand;
import me.bill.protection.configuration.Configuration;
import me.bill.protection.permission.Permissions;
import me.bill.protection.player.PlayerData;
import me.bill.protection.region.*;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

public class CommandRegion extends PlayerCommand {

    public CommandRegion() {
        super("region");
    }

    @Override
    public void onPlayerExecuted(Player player, String label, String[] args) {
        if (args.length >= 1) {
            PlayerData playerData = Protection.getInstance().getPlayerManager().getPlayerData(player);
            RegionManager regionManager = Protection.getInstance().getRegionManager();
            Configuration config = Protection.getInstance().getConfiguration();
            switch (args[0].toLowerCase()) {
                case "create":
                    if (player.hasPermission(Permissions.createRegion)) {
                        if (args.length == 2) {

                            String regionName = args[1];

                            if (!regionManager.isRegion(regionName)) {
                                SafeLocation sel1 = playerData.getSelectedLoc1();
                                SafeLocation sel2 = playerData.getSelectedLoc2();
                                if (sel1 != null && sel2 != null) {
                                    for (ChunkSet chunk : regionManager.getOverlappingChunks(sel1, sel2)) {
                                        for (Region regionInChunk : regionManager.getRegionsInChunk(chunk)) {
                                            if (regionInChunk.intersects(sel1, sel2)) {
                                                Chat.sendMessage(player, ChatType.ERROR, "Couldn't create region because it's overlapping with the region {%m}"
                                                        + regionInChunk.getName() + "{%n}!");
                                                return;
                                            }
                                        }
                                    }

                                    regionManager.createRegion(regionName, player, sel1, sel2);
                                    Chat.sendMessage(player, ChatType.SUCCESS, "Successfully created the region {%m}" + regionName + "{%n}!");
                                    regionManager.getLogger().log(Level.INFO, player.getName() + " (uuid: " + player.getUniqueId() + ") created the region '" + regionName + "'");
                                } else {
                                    Chat.sendMessage(player, ChatType.ERROR, "You haven't selected the bounds for the region!");
                                }
                            } else {
                                Chat.sendMessage(player, ChatType.ERROR, "A region by the name of {%m}" + regionName + "{%n} already exists!");
                            }

                        } else {
                            Chat.commandUsage(player, "region create <region name>", "Creates a new region");
                        }
                    } else {
                        Chat.noPerm(player, Permissions.createRegion);
                    }
                    break;
                case "delete":
                    if (player.hasPermission(Permissions.deleteRegion) || player.hasPermission(Permissions.deleteAnyRegion)) {
                        if (args.length == 2) {

                            String regionName = args[1];
                            if (regionManager.isRegion(regionName)) {
                                Region region = regionManager.getRegion(regionName);

                                if (region.getOwner().equals(player.getUniqueId()) || player.hasPermission(Permissions.deleteAnyRegion)) {
                                    regionManager.deleteRegion(region);
                                    Chat.sendMessage(player, ChatType.SUCCESS, "Successfully deleted the region {%m}" + regionName + "{%n}!");
                                    regionManager.getLogger().log(Level.WARNING, player.getName() + " (uuid: " + player.getUniqueId() + ") deleted the region '" + region.getName() + "'");
                                } else {
                                    Chat.sendMessage(player, ChatType.ERROR, "You don't own the region {%m}" + regionName + "{%n}!");
                                }
                            } else {
                                Chat.sendMessage(player, ChatType.ERROR, "No region by the name of {%m}" + regionName + "{%n} exists!");
                            }

                        } else {
                            Chat.commandUsage(player, "region delete <region name>", "Deletes one of your regions");
                        }
                    } else {
                        Chat.noPerm(player, Permissions.deleteRegion);
                    }
                    break;
                case "addflag":
                    if (player.hasPermission(Permissions.addFlag)) {
                        if (args.length == 3) {

                            String regionName = args[1];
                            if (regionManager.isRegion(regionName)) {
                                Region region = regionManager.getRegion(regionName);
                                if (region.getOwner().equals(player.getUniqueId()) || (region.isMember(player) && region.getMemberRank(player).isHigherOrEqualsThan(RegionRank.ADMIN))) {
                                    Flag flag = Flag.fromName(args[2]);
                                    if (flag != null) {
                                        if (!region.hasFlag(flag)) {
                                            region.addFlag(flag);
                                            Chat.sendMessage(player, ChatType.SUCCESS, "Successfully added the flag {%m}" + flag.name().toLowerCase() + " {%n}to the region {%m}" + region.getName() + "{%n}!");
                                            region.getLogger().log(Level.INFO, player.getName() + " (uuid: " + player.getUniqueId() + ", rank: " + (region.isOwner(player) ? "OWNER" : region.getMemberRank(player).name()) +
                                                    ") added the flag '" + flag.name() + "'");
                                        } else {
                                            Chat.sendMessage(player, ChatType.ERROR, "The region already has this flag enabled!");
                                        }
                                    } else {
                                        Chat.sendMessage(player, ChatType.ERROR, "{%m}" + args[2] + "{%n} isn't a region protection flag!");
                                        Chat.sendMessage(player, ChatType.ERROR, "Available flags: {%m}" + StringUtils.join(Flag.values(), ", ").toLowerCase());
                                    }
                                } else if (region.isMember(player) && region.getMemberRank(player).isLowerThan(RegionRank.ADMIN)) {
                                    Chat.sendMessage(player, ChatType.ERROR, "You need to be an admin of the region {%m}" + region.getName() + "{%n} to add flags!");
                                } else {
                                    Chat.sendMessage(player, ChatType.ERROR, "You don't own the region {%m}" + regionName + "{%n}!");
                                }
                            } else {
                                Chat.sendMessage(player, ChatType.ERROR, "No region by the name of {%m}" + regionName + "{%n} exists!");
                            }

                        } else {
                            Chat.commandUsage(player, "region addflag <region name> <flag name>", "Adds a protection flag to one of your regions");
                            Chat.sendMessage(player, ChatType.ERROR, "Available flags: {%m}" + StringUtils.join(Flag.values(), ", ").toLowerCase());
                        }
                    } else {
                        Chat.noPerm(player, Permissions.addFlag);
                    }
                    break;
                case "removeflag":
                    if (player.hasPermission(Permissions.removeFlag)) {
                        if (args.length == 3) {

                            String regionName = args[1];
                            if (regionManager.isRegion(regionName)) {
                                Region region = regionManager.getRegion(regionName);
                                if (region.getOwner().equals(player.getUniqueId()) || (region.isMember(player) && region.getMemberRank(player).isHigherOrEqualsThan(RegionRank.ADMIN))) {
                                    Flag flag = Flag.fromName(args[2]);
                                    if (flag != null) {
                                        if (region.hasFlag(flag)) {
                                            region.removeFlag(flag);
                                            Chat.sendMessage(player, ChatType.SUCCESS, "Successfully removed the flag {%m}" + flag.name().toLowerCase() + " {%n}from the region {%m}" + region.getName() + "{%n}!");
                                            region.getLogger().log(Level.WARNING, player.getName() + " (uuid: " + player.getUniqueId() + ", rank: " + (region.isOwner(player) ? "OWNER" : region.getMemberRank(player).name()) +
                                                    ") removed the flag '" + flag.name() + "'");
                                        } else {
                                            Chat.sendMessage(player, ChatType.ERROR, "The region doesn't have this flag enabled!");
                                        }
                                    } else {
                                        Chat.sendMessage(player, ChatType.ERROR, "{%m}" + args[2] + "{%n} isn't a region protection flag!");
                                        Chat.sendMessage(player, ChatType.ERROR, "Available flags: {%m}" + StringUtils.join(Flag.values(), ", ").toLowerCase());
                                    }
                                } else if (region.isMember(player) && region.getMemberRank(player).isLowerThan(RegionRank.ADMIN)) {
                                    Chat.sendMessage(player, ChatType.ERROR, "You need to be an admin of the region {%m}" + region.getName() + "{%n} to add flags!");
                                } else {
                                    Chat.sendMessage(player, ChatType.ERROR, "You don't own the region {%m}" + regionName + "{%n}!");
                                }
                            } else {
                                Chat.sendMessage(player, ChatType.ERROR, "No region by the name of {%m}" + regionName + "{%n} exists!");
                            }

                        } else {
                            Chat.commandUsage(player, "region removeflag <region name> <flag name>", "Removes a protection flag from one of your regions");
                            Chat.sendMessage(player, ChatType.ERROR, "Available flags: {%m}" + StringUtils.join(Flag.values(), ", ").toLowerCase());
                        }
                    } else {
                        Chat.noPerm(player, Permissions.removeFlag);
                    }
                    break;
                case "addmember":
                    if (player.hasPermission(Permissions.addMember)) {
                        if (args.length == 3) {
                            String regionName = args[1];
                            if (regionManager.isRegion(regionName)) {
                                Region region = regionManager.getRegion(regionName);
                                if (region.getOwner().equals(player.getUniqueId()) || (region.isMember(player) && region.getMemberRank(player).isHigherOrEqualsThan(RegionRank.ADMIN))) {
                                    OfflinePlayer target = Bukkit.getPlayer(args[2]);
                                    if (target == null) {
                                        target = Bukkit.getOfflinePlayer(args[2]);
                                    }

                                    if (!target.getUniqueId().equals(player.getUniqueId())) {
                                        if (!region.isMember(target.getUniqueId()) && !region.isOwner(target.getUniqueId())) {
                                            if (!region.isBanned(target.getUniqueId())) {
                                                if (target.isOnline()) {
                                                    Chat.sendMessage(target.getPlayer(), ChatType.SUCCESS, "{%m}" + player.getName() + "{%n} added you as a member to the region {%m}" + region.getName() + "{%n}!");
                                                }
                                                Chat.sendMessage(player, ChatType.SUCCESS, "Successfully added {%m}" + target.getName() + "{%n} to the region {%m}" + region.getName() + "{%n}!");
                                                region.addMember(target.getUniqueId());
                                                region.getLogger().log(Level.INFO, player.getName() + "(uuid: " + player.getUniqueId() + ") added member '" + target.getName() + " (uuid: " + target.getUniqueId() + ")'");
                                            } else {
                                                Chat.sendMessage(player, ChatType.ERROR, "The player {%m}" + target.getName() + "{%n} is banned from the region!");
                                            }
                                        } else {
                                            Chat.sendMessage(player, ChatType.ERROR, "{%m}" + target.getName() + "{%n} is already a member of the region {%m}" + region + "{%n}");
                                        }
                                    } else {
                                        Chat.sendMessage(player, ChatType.ERROR, "You can't perform this action on yourself!");
                                    }
                                } else if (region.isMember(player) && region.getMemberRank(player).isLowerThan(RegionRank.ADMIN)) {
                                    Chat.sendMessage(player, ChatType.ERROR, "You need to be an admin of the region {%m}" + region.getName() + "{%n} to add flags!");
                                } else {
                                    Chat.sendMessage(player, ChatType.ERROR, "You don't own the region {%m}" + regionName + "{%n}!");
                                }
                            } else {
                                Chat.sendMessage(player, ChatType.ERROR, "No region by the name of {%m}" + regionName + "{%n} exists!");
                            }
                        } else {
                            Chat.commandUsage(player, "region addmember <region name> <player name>", "Adds a member to one of your regions");
                        }
                    } else {
                        Chat.noPerm(player, Permissions.addMember);
                    }
                    break;
                case "kickmember":
                    if (player.hasPermission(Permissions.kickMember)) {
                        if (args.length == 3) {
                            String regionName = args[1];
                            if (regionManager.isRegion(regionName)) {
                                Region region = regionManager.getRegion(regionName);
                                if (region.isOwner(player) || (region.isMember(player) && region.getMemberRank(player).isHigherOrEqualsThan(RegionRank.ADMIN))) {
                                    OfflinePlayer target = Bukkit.getPlayer(args[2]);
                                    if (target == null) {
                                        target = Bukkit.getOfflinePlayer(args[2]);
                                    }

                                    if (!target.getUniqueId().equals(player.getUniqueId())) {
                                        if (region.isMember(target.getUniqueId())) {
                                            if (region.isOwner(player) || region.getMemberRank(target.getUniqueId()).isLowerThan(region.getMemberRank(player))) {
                                                region.removeMember(target.getUniqueId());
                                                if (target.getPlayer() != null) {
                                                    Chat.sendMessage(target.getPlayer(), ChatType.ERROR, "{%m}" + player.getName() + "{%n} kicked you from the region {%m}" + region.getName() + "{%n}!");
                                                }
                                                Chat.sendMessage(player, ChatType.SUCCESS, "Successfully kicked {%m}" + target.getName() + "{%n} from the region {%m}" + region.getName() + "{%n}!");
                                                region.getLogger().log(Level.WARNING, player.getName() + "(uuid: " + player.getUniqueId() + ") kicked member '" + target.getName() + " (uuid: " + target.getUniqueId() + ")'");
                                            } else {
                                                Chat.sendMessage(player, ChatType.ERROR, "You can't kick someone that is the same or a higher region rank than you!");
                                            }
                                        } else {
                                            Chat.sendMessage(player, ChatType.ERROR, "{%m}" + target.getName() + "{%n} isn't a member of the region {%m}" + region + "{%n}");
                                        }
                                    } else {
                                        Chat.sendMessage(player, ChatType.ERROR, "You can't perform this action on yourself!");
                                    }
                                } else if (region.isMember(player) && region.getMemberRank(player).isLowerThan(RegionRank.ADMIN)) {
                                    Chat.sendMessage(player, ChatType.ERROR, "You need to be an admin of the region {%m}" + region.getName() + "{%n} to add flags!");
                                } else {
                                    Chat.sendMessage(player, ChatType.ERROR, "You don't own the region {%m}" + regionName + "{%n}!");
                                }
                            } else {
                                Chat.sendMessage(player, ChatType.ERROR, "No region by the name of {%m}" + regionName + "{%n} exists!");
                            }
                        } else {
                            Chat.commandUsage(player, "region kickmember <region name> <player name>", "Removes a member from one of your regions");
                        }
                    } else {
                        Chat.noPerm(player, Permissions.addMember);
                    }
                    break;
                case "setrank":
                    if (player.hasPermission(Permissions.setRank)) {
                        if (args.length == 4) {
                            String regionName = args[1];
                            if (regionManager.isRegion(regionName)) {
                                Region region = regionManager.getRegion(regionName);
                                if (region.isOwner(player)) {
                                    OfflinePlayer target = Bukkit.getPlayer(args[2]);
                                    if (target == null) {
                                        target = Bukkit.getOfflinePlayer(args[2]);
                                    }

                                    if (!target.getUniqueId().equals(player.getUniqueId())) {
                                        if (region.isMember(target.getUniqueId())) {
                                            RegionRank rank = RegionRank.fromName(args[3]);
                                            if (rank != null) {
                                                region.setRank(target.getUniqueId(), rank);
                                                Chat.sendMessage(player, ChatType.SUCCESS, "You set {%m}" + target.getName() + "{%n}'s region rank to {%m}" + rank.name().toLowerCase() + "{%n} for the region {%m}" + region.getName() + "{%n}!");
                                                if (target.isOnline()) {
                                                    Chat.sendMessage(target.getPlayer(), ChatType.SUCCESS, "{%m}" + player.getName() + "{%n} set your region rank to {%m}" + rank.name().toLowerCase() + "{%n} for the region {%m}" + region.getName() + "{%n}!");
                                                }
                                                region.getLogger().log(Level.INFO, player.getName() + " (uuid: " + player.getUniqueId() + ") changed the rank of the member '" + target.getName() + " (uuid: " + target.getUniqueId() + ")' to '" + rank.name() + "'");
                                            } else {
                                                Chat.sendMessage(player, ChatType.ERROR, "The region rank {%m}" + args[3] + "{%n} doesn't exist!");
                                            }
                                        } else {
                                            Chat.sendMessage(player, ChatType.ERROR, "{%m}" + target.getName() + "{%n} isn't a member of the region {%m}" + region + "{%n}");
                                        }
                                    } else {
                                        Chat.sendMessage(player, ChatType.ERROR, "You can't perform this action on yourself!");
                                    }
                                } else {
                                    Chat.sendMessage(player, ChatType.ERROR, "You don't own the region {%m}" + regionName + "{%n}!");
                                }
                            } else {
                                Chat.sendMessage(player, ChatType.ERROR, "No region by the name of {%m}" + regionName + "{%n} exists!");
                            }
                        } else {
                            Chat.commandUsage(player, "region setrank <region name> <member name> <rank>", "Sets the rank of a member of one of your reiongs");
                        }
                    } else {
                        Chat.noPerm(player, Permissions.setRank);
                    }
                    break;
                case "info":
                    if (player.hasPermission(Permissions.regionInfo)) {
                        if (args.length == 1 || args.length == 2) {
                            Region region = null;
                            if (args.length == 1) {
                                for (Region regionInChunk : regionManager.getRegionsInChunk(player.getLocation().getChunk())) {
                                    if (regionInChunk.isInRegion(player)) {
                                        region = regionInChunk;
                                        break;
                                    }
                                }
                                if (region == null) {
                                    Chat.sendMessage(player, ChatType.ERROR, "You aren't standing in a region!");
                                    return;
                                }
                            } else {
                                String regionName = args[1];
                                if (regionManager.isRegion(regionName)) {
                                    region = regionManager.getRegion(regionName);
                                } else {
                                    Chat.sendMessage(player, ChatType.ERROR, "No region by the name of {%m}" + regionName + "{%n} exists!");
                                    return;
                                }
                            }

                            Chat.sendMessage(player, ChatType.SUCCESS, "Showing info for the region {%m}" + region.getName() + "{%n}:");
                            Chat.sendMessage(player, ChatType.SUCCESS, "Owner: {%m}" + Bukkit.getOfflinePlayer(region.getOwner()).getName());

                            int adminCount = 0;
                            StringBuilder admins = new StringBuilder();
                            int memberCount = 0;
                            StringBuilder members = new StringBuilder();
                            for (Map.Entry<UUID, RegionRank> entry : region.getMembers().entrySet()) {
                                if (entry.getValue() == RegionRank.ADMIN) {
                                    if (admins.length() > 0) {
                                        admins.append(", ");
                                    }
                                    admins.append(Bukkit.getOfflinePlayer(entry.getKey()).getName());
                                    adminCount++;
                                } else {
                                    if (members.length() > 0) {
                                        members.append(", ");
                                    }
                                    members.append(Bukkit.getOfflinePlayer(entry.getKey()).getName());
                                    memberCount++;
                                }
                            }

                            Chat.sendMessage(player, ChatType.SUCCESS, "Admins (" + adminCount + "): {%m}" + admins.toString());
                            Chat.sendMessage(player, ChatType.SUCCESS, "Members (" + memberCount + "): {%m}" + members.toString());
                            Chat.sendMessage(player, ChatType.SUCCESS, "Flags (" + region.getFlags().size() + "): {%m}" + StringUtils.join(region.getFlags(), ", ").toLowerCase());
                        } else {
                            Chat.commandUsage(player, "region info (region name)", "Shows the info of the region you're standing in, or a specific region");
                        }
                    } else {
                        Chat.noPerm(player, Permissions.regionInfo);
                    }
                    break;
                case "ban":
                    if (player.hasPermission(Permissions.regionBanPlayer)) {
                        if (args.length == 3) {
                            String regionName = args[1];
                            if (regionManager.isRegion(regionName)) {
                                Region region = regionManager.getRegion(regionName);
                                if (region.isOwner(player) || ((region.isMember(player) && region.getMemberRank(player).isHigherOrEqualsThan(RegionRank.ADMIN)) && config.adminsCanBan())) {
                                    OfflinePlayer target = Bukkit.getPlayer(args[2]);
                                    if (target == null) {
                                        target = Bukkit.getOfflinePlayer(args[2]);
                                    }

                                    if (!target.getUniqueId().equals(player.getUniqueId())) {
                                        if (!region.isBanned(target.getUniqueId())) {
                                            if (region.isOwner(target.getUniqueId())) {
                                                Chat.sendMessage(player, ChatType.ERROR, "You can't ban the owner of the region!");
                                                return;
                                            } else if (!region.isOwner(player) && region.isMember(target.getUniqueId()) && region.getMemberRank(target.getUniqueId()).isHigherOrEqualsThan(RegionRank.ADMIN)) {
                                                Chat.sendMessage(player, ChatType.ERROR, "You can't ban someone that is the same or a higher region rank in the region {%m}" + region.getName() + "{%n}!");
                                                return;
                                            }

                                            if (region.isMember(target.getUniqueId())) {
                                                region.removeMember(target.getUniqueId());
                                            }
                                            if (target.isOnline()) {
                                                Chat.sendMessage(target.getPlayer(), ChatType.ERROR, "{%m}" + player.getName() + "{%n} banned you from the region {%n}" + region.getName());
                                            }
                                            Chat.sendMessage(player, ChatType.SUCCESS, "You banned {%m}" + target.getName() + "{%n} from the region {%m}" + region.getName() + "{%n}!");
                                            region.getLogger().log(Level.INFO, player.getName() + " (uuid: " + player.getUniqueId() + ", rank: " + (region.isOwner(player) ? "OWNER" : region.getMemberRank(player).name()) +
                                                    ") banned the player '" + target.getName() + " (uuid: " + target.getUniqueId() + ")'");
                                            region.banPlayer(target.getUniqueId());
                                        } else {
                                            Chat.sendMessage(player, ChatType.ERROR, "{%m}" + target.getName() + "{%n} is already banned from the region {%m}" + region.getName() + "{%n}!");
                                        }
                                    } else {
                                        Chat.sendMessage(player, ChatType.ERROR, "You can't perform this action on yourself!");
                                    }
                                } else {
                                    Chat.sendMessage(player, ChatType.ERROR, "You don't own the region {%m}" + regionName + "{%n}!");
                                }
                            } else {
                                Chat.sendMessage(player, ChatType.ERROR, "No region by the name of {%m}" + regionName + "{%n} exists!");
                            }
                        } else {
                            Chat.commandUsage(player, "region ban <region name> <player name>", "Bans a player from your region.");
                        }
                    } else {
                        Chat.noPerm(player, Permissions.regionBanPlayer);
                    }
                    break;
                case "unban":
                    if (player.hasPermission(Permissions.regionUnbanPlayer)) {
                        if (args.length == 3) {
                            String regionName = args[1];
                            if (regionManager.isRegion(regionName)) {
                                Region region = regionManager.getRegion(regionName);
                                if (region.isOwner(player) || ((region.isMember(player) && region.getMemberRank(player).isHigherOrEqualsThan(RegionRank.ADMIN)) && config.adminsCanUnban())) {
                                    OfflinePlayer target = Bukkit.getPlayer(args[2]);
                                    if (target == null) {
                                        target = Bukkit.getOfflinePlayer(args[2]);
                                    }

                                    if (!target.getUniqueId().equals(player.getUniqueId())) {
                                        if (region.isBanned(target.getUniqueId())) {
                                            if (target.isOnline()) {
                                                Chat.sendMessage(target.getPlayer(), ChatType.ERROR, "{%m}" + player.getName() + "{%n} un-banned you from the region {%n}" + region.getName());
                                            }
                                            Chat.sendMessage(player, ChatType.SUCCESS, "You banned {%m}" + target.getName() + "{%n} from the region {%m}" + region.getName() + "{%n}!");
                                            region.getLogger().log(Level.INFO, player.getName() + " (uuid: " + player.getUniqueId() + ", rank: " + (region.isOwner(player) ? "OWNER" : region.getMemberRank(player).name()) +
                                                    ") un-banned the player '" + target.getName() + " (uuid: " + target.getUniqueId() + ")'");
                                            region.unBanPlayer(target.getUniqueId());
                                        } else {
                                            Chat.sendMessage(player, ChatType.ERROR, "{%m}" + target.getName() + "{%n} isn't banned from the region {%m}" + region.getName() + "{%n}!");
                                        }
                                    } else {
                                        Chat.sendMessage(player, ChatType.ERROR, "You can't perform this action on yourself!");
                                    }
                                } else {
                                    Chat.sendMessage(player, ChatType.ERROR, "You don't own the region {%m}" + regionName + "{%n}!");
                                }
                            } else {
                                Chat.sendMessage(player, ChatType.ERROR, "No region by the name of {%m}" + regionName + "{%n} exists!");
                            }
                        } else {
                            Chat.commandUsage(player, "region unban <region name> <player name>", "Unbans a player from your region.");
                        }
                    } else {
                        Chat.noPerm(player, Permissions.regionUnbanPlayer);
                    }
                    break;
                case "rename":
                    if (player.hasPermission(Permissions.renameRegion)) {
                        if (args.length == 3) {
                            String regionName = args[1];
                            if (regionManager.isRegion(regionName)) {
                                Region region = regionManager.getRegion(regionName);

                                if (region.isOwner(player)) {
                                    String newName = args[2];
                                    if (!regionManager.isRegion(newName)) {
                                        Chat.sendMessage(player, ChatType.SUCCESS, "You renamed the region {%m}" + region.getName() + "{%n} to {%m}" + newName + "{%n}!");
                                        regionManager.getLogger().log(Level.INFO, player.getName() + " (uuid: " + player.getUniqueId() + ") renamed the region '" + region.getName() + "' to '" + newName + "'");
                                        regionManager.renameRegion(region, newName);
                                    } else {
                                        Chat.sendMessage(player, ChatType.ERROR, "A region by the name of {%m}" + regionName + "{%n} already exists!");
                                    }
                                } else {
                                    Chat.sendMessage(player, ChatType.ERROR, "You don't own the region {%m}" + regionName + "{%n}!");
                                }
                            } else {
                                Chat.sendMessage(player, ChatType.ERROR, "No region by the name of {%m}" + regionName + "{%n} exists!");
                            }
                        } else {
                            Chat.commandUsage(player, "region rename <region name> <new name>", "Renames one of of your regions");
                        }
                    } else {
                        Chat.noPerm(player, Permissions.renameRegion);
                    }
                    break;
                case "help":
                    sendHelp(player);
                    break;
                default:
                    sendHelp(player);
                    break;
            }
        } else {
            sendHelp(player);
        }
    }

    private void sendHelp(Player player) {
        if (player.hasPermission(Permissions.regionHelp)) {
            Chat.sendMessage(player, ChatType.HELP, "Showing help for the command {%m}/region{%n}:");
            Chat.commandHelp(player, "region help", "Shows you the help for all the region sub commands");
            if (player.hasPermission(Permissions.createRegion)) {
                Chat.commandHelp(player, "region create <region name>", "Creates a new region if the name isn't already taken. You must select the 2 bounds for the region in order for this to work.");
            }
            if (player.hasPermission(Permissions.deleteRegion) || player.hasPermission(Permissions.deleteAnyRegion)) {
                Chat.commandHelp(player, "region delete <region name>", "Deletes a region if a region by the given name exists.");
            }
            if (player.hasPermission(Permissions.addFlag)) {
                Chat.commandHelp(player, "region addflag <region name> <flag name>", "Adds a flag to a specific region. Type /flag to learn more about flags.");
            }
            if (player.hasPermission(Permissions.removeFlag)) {
                Chat.commandHelp(player, "region removeflag <region name> <flag name>", "Removes a flag from a specific region. Type /flag to learn more about flags.");
            }
            if (player.hasPermission(Permissions.addMember)) {
                Chat.commandHelp(player, "region addmember <region name> <player name>", "Adds a member to a specific region. This member can then bypass the protection flags.");
            }
            if (player.hasPermission(Permissions.kickMember)) {
                Chat.commandHelp(player, "region kickmember <region name> <member name>", "Kicks a member from a specific region.");
            }
            if (player.hasPermission(Permissions.setRank)) {
                Chat.commandHelp(player, "region setrank <region name> <member name> <rank name>", "Sets the region rank for a member of a specific region. Only the owner can do this.");
            }
            if (player.hasPermission(Permissions.regionBanPlayer)) {
                Chat.commandHelp(player, "region ban <region name> <player name>", "Bans a player from a region. This player can not be added as a member to the region as long as he's banned.");
            }
            if (player.hasPermission(Permissions.regionUnbanPlayer)) {
                Chat.commandHelp(player, "region unban <region name> <player name>", "Unbans a player from a region. This player can now be added as a member again.");
            }
            if (player.hasPermission(Permissions.regionInfo)) {
                Chat.commandHelp(player, "region info (region name)", "Shows information about a specific region. If no region name is given it will show you the information of the region you're standing in.");
            }
            if (player.hasPermission(Permissions.renameRegion)) {
                Chat.commandHelp(player, "region rename <region name> <new name>", "Renames an existing region to a name that isn't already taken.");
            }
        } else {
            Chat.noPerm(player, Permissions.regionHelp);
        }
    }
}