package me.bill.protection.command.defaults;

import me.bill.protection.Protection;
import me.bill.protection.chat.Chat;
import me.bill.protection.chat.ChatType;
import me.bill.protection.command.PlayerCommand;
import me.bill.protection.permission.Permissions;
import me.bill.protection.player.PlayerData;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class CommandSelection extends PlayerCommand {

    public CommandSelection() {
        super("selection");
    }

    @Override
    public void onPlayerExecuted(Player player, String label, String[] args) {
        if (args.length > 0) {
            PlayerData playerData = Protection.getInstance().getPlayerManager().getPlayerData(player);
            switch (args[0].toLowerCase()) {
                case "toggle":
                    if (player.hasPermission(Permissions.selectionToggle)) {
                        if (args.length == 1) {
                            playerData.setInSelectionMode(!playerData.isInSelectionMode());
                            if (playerData.isInSelectionMode()) {
                                Chat.sendMessage(player, ChatType.SUCCESS, "You are now {%m}in{%n} selection mode!");
                            } else {
                                Chat.sendMessage(player, ChatType.ERROR, "You are now {%m}out{%n} of selection mode!");
                            }
                        } else {
                            Chat.commandUsage(player, "selection toggle", "Toggles whether you are in selection mode.");
                        }
                    } else {
                        Chat.noPerm(player, Permissions.selectionToggle);
                    }
                    break;
                case "tool":
                    if (args.length == 1) {
                        if (player.hasPermission(Permissions.selectionToolView)) {
                            Material tool = playerData.getSelectionTool() != null ? playerData.getSelectionTool() : Protection.getInstance().getConfiguration().getSelectionTool();
                            Chat.sendMessage(player, ChatType.SUCCESS, "Current selection tool: {%m}" + tool.name().toLowerCase().replace("_", " ") + " (" + tool.getId() + ")");
                        } else {
                            Chat.noPerm(player, Permissions.selectionToggle);
                        }
                    } else if (args.length == 2) {
                        if (player.hasPermission(Permissions.selectionToolSet)) {
                            if (isInt(args[1])) {
                                Material item = Material.getMaterial(Integer.parseInt(args[1]));
                                if (item != null) {
                                    playerData.setSelectionTool(item);
                                    Chat.sendMessage(player, ChatType.SUCCESS, "You changed your selection tool to {%m}" + item.name().toLowerCase().replace("_", " ") + " (" + args[1] + ")");
                                } else {
                                    Chat.sendMessage(player, ChatType.ERROR, "No item by the id {%m}" + args[1] + "{%n} exists!");
                                }
                            } else {
                                Chat.sendMessage(player, ChatType.ERROR, "The item id must be a number! (given {%m}" + args[1] + "{%n})");
                            }
                        } else {
                            Chat.noPerm(player, Permissions.selectionToggle);
                        }
                    } else if (player.hasPermission(Permissions.selectionToolSet)) {
                        Chat.commandUsage(player, "selection tool (item id)", "Sets or views your current selection tool item.");
                    } else if (player.hasPermission(Permissions.selectionToolView)) {
                        Chat.commandUsage(player, "selection tool", "Views your current selection tool item.");
                    } else {
                        Chat.noPerm(player, Permissions.selectionToolView);
                    }
                    break;
                case "help":
                default:
                    sendHelp(player);
                    break;
            }
        } else {
            sendHelp(player);
        }
    }

    private void sendHelp(Player player) {
        if (player.hasPermission(Permissions.selectionHelp)) {
            Chat.sendMessage(player, ChatType.HELP, "Showing help for the command {%m}/selection{%n}:");
            Chat.commandHelp(player, "selection help", "Shows all the available sub-commands for /selection");
            if (player.hasPermission(Permissions.selectionToggle)) {
                Chat.commandHelp(player, "selection toggle", "Toggles whether you are in selection mode. If you are in selection mode you can select the bounds for a region.");
            }
            if (player.hasPermission(Permissions.selectionToolView)) {
                Chat.commandHelp(player, "selection tool", "Shows you what item you currently have as the selection tool. You will use this tool item in selection mode to select bounds for a region.");
            }
            if (player.hasPermission(Permissions.selectionToolSet)) {
                Chat.commandHelp(player, "selection tool <item id>", "Sets your selection tool, you will use this tool item in selection mode to select bounds for a region.");
            }
        } else {
            Chat.noPerm(player, Permissions.selectionHelp);
        }
    }
}
