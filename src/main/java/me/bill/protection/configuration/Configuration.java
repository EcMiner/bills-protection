package me.bill.protection.configuration;

import me.bill.protection.Protection;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

public class Configuration {

    private final FileConfiguration configFile;
    private Material selectionTool = Material.BLAZE_ROD;
    private boolean adminsCanBan = false;
    private boolean adminsCanUnban = false;

    public Configuration(FileConfiguration configFile) {
        this.configFile = configFile;
        load();
    }

    private void load() {
        this.selectionTool = Material.getMaterial(configFile.getInt("selection-tool", 369));
        this.adminsCanBan = configFile.getBoolean("admins-can-ban", false);
        this.adminsCanUnban = configFile.getBoolean("admins-can-unban", false);
    }

    public void setSelectionTool(Material selectionTool) {
        this.selectionTool = selectionTool;
        configFile.set("selection-tool", selectionTool.getId());
        saveConfig();
    }

    public Material getSelectionTool() {
        return selectionTool;
    }

    private void saveConfig() {
        Protection.getInstance().saveConfig();
    }

    public void setAdminsCanBan(boolean adminsCanBan) {
        this.adminsCanBan = adminsCanBan;
        configFile.set("admins-can-ban", adminsCanBan);
        saveConfig();
    }

    public boolean adminsCanBan() {
        return adminsCanBan;
    }

    public boolean adminsCanUnban() {
        return adminsCanUnban;
    }

    public void setAdminsCanUnban(boolean adminsCanUnban) {
        this.adminsCanUnban = adminsCanUnban;
        configFile.set("admins-can-unban", adminsCanUnban);
        saveConfig();
    }
}
