package me.bill.protection.database;

import me.bill.protection.player.PlayerData;
import me.bill.protection.region.Flag;
import me.bill.protection.region.Region;
import me.bill.protection.region.RegionRank;
import org.bukkit.Material;

import java.util.List;
import java.util.UUID;

public interface Database {

    List<Region> loadAllRegions();

    void createRegion(Region region);

    void deleteRegion(Region region);

    void renameRegion(String regionName, String newName);

    void addFlag(Region region, Flag flag);

    void removeFlag(Region region, Flag flag);

    void addMember(Region region, UUID playerId);

    void removeMember(Region region, UUID playerId);

    void setMemberRank(Region region, UUID playerId, RegionRank rank);

    PlayerData loadPlayerData(UUID playerId);

    void setPlayerSelectionTool(UUID playerId, Material selectionTool);

    void createPlayerData(UUID playerId, Material selectionTool);

    void banPlayer(Region region, UUID playerId);

    void unBanPlayer(Region region, UUID playerId);

    void unload();

}
