package me.bill.protection.database;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import me.bill.protection.player.PlayerData;
import me.bill.protection.region.Flag;
import me.bill.protection.region.Region;
import me.bill.protection.region.RegionRank;
import me.bill.protection.region.SafeLocation;
import org.bson.Document;
import org.bukkit.Material;

import java.util.*;

public class MongoDB implements Database {

    private final String databaseName;
    private MongoClient mongo;
    private MongoDatabase database;

    private final String collectionRegions = "regions";
    private final String collectionPlayerData = "player_data";

    public MongoDB(String host, int port, String username, String password, String authDatabase, String databaseName) {
        this.databaseName = databaseName;
        MongoCredential auth = MongoCredential.createCredential(username, authDatabase, password.toCharArray());
        mongo = new MongoClient(new ServerAddress(host, port), Arrays.asList(auth));
        prepareDatabase();
    }

    private void prepareDatabase() {
        this.database = mongo.getDatabase(databaseName);
    }

    @Override
    public List<Region> loadAllRegions() {
        List<Region> regions = new ArrayList<Region>();

        MongoCollection<Document> collection = database.getCollection(collectionRegions);
        for (Document doc : collection.find()) {
            if (doc.containsKey("name") && doc.containsKey("owner") && doc.containsKey("location1") && doc.containsKey("location2")) {
                Region region = new Region(doc.getString("name"), UUID.fromString(doc.getString("owner")), SafeLocation.fromString(doc.getString("location1")),
                        SafeLocation.fromString(doc.getString("location2")));

                region.getMembers().putAll(getMembers(doc));
                region.getFlags().addAll(getFlags(doc));
                region.getBannedPlayers().addAll(getBannedPlayers(doc));

                regions.add(region);
            }
        }

        return regions;
    }

    @Override
    public void createRegion(Region region) {
        database.getCollection(collectionRegions).insertOne(new Document("name", region.getName()).append("lookup_name", region.getName().toLowerCase())
                .append("owner", region.getOwner().toString()).append("location1", region.getMax().toString()).append("location2", region.getMin().toString()));
    }

    @Override
    public void deleteRegion(Region region) {
        database.getCollection(collectionRegions).deleteOne(new Document("lookup_name", region.getName().toLowerCase()));
    }

    @Override
    public void renameRegion(String regionName, String newName) {
        database.getCollection(collectionRegions).updateOne(new Document("lookup_name", regionName.toLowerCase()), new Document("$set",
                new Document("name", newName).append("lookup_name", newName.toLowerCase())));
    }

    @Override
    public void addFlag(Region region, Flag flag) {
        database.getCollection(collectionRegions).updateOne(new Document("lookup_name", region.getName().toLowerCase()), new Document("$push",
                new Document("flags", flag.name())));
    }

    @Override
    public void removeFlag(Region region, Flag flag) {
        database.getCollection(collectionRegions).updateOne(new Document("lookup_name", region.getName().toLowerCase()), new Document("$pull",
                new Document("flags", flag.name())));
    }

    @Override
    public void addMember(Region region, UUID playerId) {
        database.getCollection(collectionRegions).updateOne(new Document("lookup_name", region.getName().toLowerCase()), new Document("$push",
                new Document("members", new Document("uuid", playerId.toString()).append("rank", RegionRank.MEMBER.name()))));
    }

    @Override
    public void removeMember(Region region, UUID playerId) {
        database.getCollection(collectionRegions).updateOne(new Document("lookup_name", region.getName().toLowerCase()), new Document("$pull",
                new Document("members", new Document("uuid", playerId.toString()))));
    }

    @Override
    public void setMemberRank(Region region, UUID playerId, RegionRank rank) {
        database.getCollection(collectionRegions).updateOne(new Document("lookup_name", region.getName().toLowerCase()).append("members.uuid", playerId.toString()),
                new Document("$set", new Document("members.$.rank", rank.name())));
    }

    @Override
    public PlayerData loadPlayerData(UUID playerId) {
        Document doc = database.getCollection(collectionPlayerData).find(new Document("uuid", playerId.toString())).first();
        if (doc != null) {
            PlayerData playerData = new PlayerData(playerId, true);

            if (doc.containsKey("tool")) {
                Material tool = Material.getMaterial(doc.getInteger("tool", 277));
                if (tool != null) {
                    playerData.setSelectionTool(tool, false);
                }
            }
            return playerData;
        }
        return null;
    }

    @Override
    public void setPlayerSelectionTool(UUID playerId, Material selectionTool) {
        database.getCollection(collectionPlayerData).updateOne(new Document("uuid", playerId.toString()), new Document("$set",
                new Document("tool", selectionTool.getId())));
    }

    @Override
    public void createPlayerData(UUID playerId, Material selectionTool) {
        database.getCollection(collectionPlayerData).insertOne(new Document("uuid", playerId.toString()).append("tool", selectionTool.getId()));
    }

    @Override
    public void banPlayer(Region region, UUID playerId) {
        database.getCollection(collectionRegions).updateOne(new Document("lookup_name", region.getName().toLowerCase()), new Document("$push",
                new Document("banned", playerId.toString())));
    }

    @Override
    public void unBanPlayer(Region region, UUID playerId) {
        database.getCollection(collectionRegions).updateOne(new Document("lookup_name", region.getName().toLowerCase()), new Document("$pull",
                new Document("banned", playerId.toString())));
    }

    private Map<UUID, RegionRank> getMembers(Document region) {
        Map<UUID, RegionRank> members = new HashMap<>();

        if (region.containsKey("members") && region.get("members") instanceof List) {
            for (Object obj : region.get("members", List.class)) {
                if (obj instanceof Document) {
                    Document member = (Document) obj;
                    if (member.containsKey("uuid") && member.containsKey("rank")) {
                        members.put(UUID.fromString(member.getString("uuid")), RegionRank.fromName(member.getString("rank")));
                    }
                }
            }
        }

        return members;
    }

    private List<Flag> getFlags(Document region) {
        List<Flag> flags = new ArrayList<>();

        if (region.containsKey("flags") && region.get("flags") instanceof List) {
            for (Object obj : region.get("flags", List.class)) {
                if (obj instanceof String) {
                    Flag flag = Flag.fromName((String) obj);
                    if (flag != null) {
                        flags.add(flag);
                    }
                }
            }
        }

        return flags;
    }

    private List<UUID> getBannedPlayers(Document region) {
        List<UUID> bannedPlayers = new ArrayList<>();

        if (region.containsKey("banned") && region.get("banned") instanceof List) {
            for (Object obj : region.get("banned", List.class)) {
                if (obj instanceof String) {
                    bannedPlayers.add(UUID.fromString((String) obj));
                }
            }
        }

        return bannedPlayers;
    }

    @Override
    public void unload() {
        database = null;
        mongo.close();
    }
}
