package me.bill.protection.database;

import me.bill.protection.player.PlayerData;
import me.bill.protection.region.Flag;
import me.bill.protection.region.Region;
import me.bill.protection.region.RegionRank;
import me.bill.protection.region.SafeLocation;
import org.bukkit.Material;

import java.sql.*;
import java.util.*;

public class MySQL implements Database {

    private Connection connection;
    private final String databaseName;

    private final String tableRegions = "regions";
    private final String tableFlags = "region_flags";
    private final String tableMembers = "region_members";
    private final String tablePlayerData = "player_data";
    private final String tableBannedPlayers = "region_banned";

    public MySQL(String host, int port, String username, String password, String databaseName) {
        this.databaseName = databaseName;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port, username, password);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        prepareDatabase();
    }

    private void prepareDatabase() {
        execute("CREATE DATABASE IF NOT EXISTS " + databaseName + ";");
        execute("USE " + databaseName + ";");
        execute("CREATE TABLE IF NOT EXISTS " + tableRegions + " (name VARCHAR(40), owner CHAR(36), location1 VARCHAR(255), location2 VARCHAR(255), PRIMARY KEY (name));");
        execute("CREATE TABLE IF NOT EXISTS " + tableFlags + "(region VARCHAR(40), flag VARCHAR(100), PRIMARY KEY(region, flag), FOREIGN KEY (region) REFERENCES " + tableRegions + " (name));");
        execute("CREATE TABLE IF NOT EXISTS " + tableMembers + "(uuid CHAR(36), region VARCHAR(40), rank VARCHAR(40), PRIMARY KEY(uuid, region), FOREIGN KEY (region) REFERENCES " + tableRegions + "(name));");
        execute("CREATE TABLE IF NOT EXISTS " + tablePlayerData + "(uuid CHAR(36), tool INT(11) DEFAULT NULL, PRIMARY KEY(uuid));");
        execute("CREATE TABLE IF NOT EXISTS " + tableBannedPlayers + "(region VARCHAR(40), uuid CHAR(36), PRIMARY KEY(region, uuid), FOREIGN KEY (region) REFERENCES " + tableRegions + "(name));");
    }

    private int execute(String query, Object... values) {
        try {

            PreparedStatement statement = connection.prepareStatement(query);

            for (int i = 0; i < values.length; i++) {
                Object obj = values[i];
                statement.setObject(i + 1, obj);
            }

            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private ResultSet query(String query, Object... values) {
        try {
            PreparedStatement statement = connection.prepareStatement(query);

            for (int i = 0; i < values.length; i++) {
                Object obj = values[i];
                statement.setObject(i + 1, obj);
            }

            return statement.executeQuery();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Region> loadAllRegions() {
        List<Region> regions = new ArrayList<Region>();

        ResultSet result = query("SELECT * FROM " + tableRegions + ";");
        try {
            while (result != null && result.next()) {
                Region region = new Region(result.getString("name"), UUID.fromString(result.getString("owner")),
                        SafeLocation.fromString(result.getString("location1")), SafeLocation.fromString(result.getString("location2")));

                region.getFlags().addAll(getFlags(region));
                region.getMembers().putAll(getMembers(region));
                region.getBannedPlayers().addAll(getBannedPlayers(region));

                regions.add(region);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return regions;
    }

    @Override
    public void createRegion(Region region) {
        execute("INSERT IGNORE INTO " + tableRegions + "(name, owner, location1, location2) VALUE(?, ?, ?, ?);",
                region.getName(), region.getOwner().toString(), region.getMax().toString(), region.getMin().toString());
    }

    @Override
    public void deleteRegion(Region region) {
        execute("DELETE FROM " + tableBannedPlayers + " WHERE region=?;", region.getName());
        execute("DELETE FROM " + tableFlags + " WHERE region=?;", region.getName());
        execute("DELETE FROM " + tableMembers + " WHERE region=?;", region.getName());
        execute("DELETE FROM " + tableRegions + " WHERE name=?;", region.getName());
    }

    @Override
    public void renameRegion(String regionName, String newName) {
        execute("SET foreign_key_checks=?;", 0);
        execute("UPDATE " + tableRegions + " SET name=? WHERE name=?;", newName, regionName);
        execute("UPDATE " + tableMembers + " SET region=? WHERE region=?;", newName, regionName);
        execute("UPDATE " + tableFlags + " SET region=? WHERE region=?;", newName, regionName);
        execute("UPDATE " + tableBannedPlayers + " SET region=? WHERE region=?;", newName, regionName);
        execute("SET foreign_key_checks=?;", 1);
    }

    @Override
    public void addFlag(Region region, Flag flag) {
        execute("INSERT IGNORE INTO " + tableFlags + "(region, flag) VALUE(?, ?);", region.getName(), flag.name());
    }

    @Override
    public void removeFlag(Region region, Flag flag) {
        execute("DELETE FROM " + tableFlags + " WHERE region=? AND flag=?;", region.getName(), flag.name());
    }

    @Override
    public void addMember(Region region, UUID playerId) {
        execute("INSERT IGNORE INTO " + tableMembers + "(uuid, region, rank) VALUE(?, ?, ?);", playerId.toString(), region.getName(), RegionRank.MEMBER.name());
    }

    @Override
    public void removeMember(Region region, UUID playerId) {
        execute("DELETE FROM " + tableMembers + " WHERE uuid=? AND region=?;", playerId.toString(), region.getName());
    }

    @Override
    public void setMemberRank(Region region, UUID playerId, RegionRank rank) {
        execute("UPDATE " + tableMembers + " SET rank=? WHERE uuid=?;", rank.name(), playerId.toString());
    }

    @Override
    public PlayerData loadPlayerData(UUID playerId) {
        ResultSet result = query("SELECT * FROM " + tablePlayerData + " WHERE uuid=?;", playerId.toString());
        try {
            if (result != null && result.next()) {
                PlayerData playerData = new PlayerData(playerId, true);

                if (result.getObject("tool") != null) {
                    Material tool = Material.getMaterial(result.getInt("tool"));
                    if (tool != null) {
                        playerData.setSelectionTool(tool, false);
                    }
                }
                return playerData;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void setPlayerSelectionTool(UUID playerId, Material selectionTool) {
        execute("UPDATE " + tablePlayerData + " SET tool=? WHERE uuid=?;", selectionTool.getId(), playerId.toString());
    }

    @Override
    public void createPlayerData(UUID playerId, Material selectionTool) {
        execute("INSERT IGNORE INTO " + tablePlayerData + "(uuid, tool) VALUE(?, ?);", playerId.toString(), selectionTool.getId());
    }

    @Override
    public void banPlayer(Region region, UUID playerId) {
        execute("INSERT IGNORE INTO " + tableBannedPlayers + "(region, uuid) VALUE (?, ?);", region.getName(), playerId.toString());
    }

    @Override
    public void unBanPlayer(Region region, UUID playerId) {
        execute("DELETE FROM " + tableBannedPlayers + " WHERE region=? AND uuid=?;", region.getName(), playerId.toString());
    }

    private Map<UUID, RegionRank> getMembers(Region region) {
        Map<UUID, RegionRank> members = new HashMap<UUID, RegionRank>();

        ResultSet result = query("SELECT uuid, rank FROM " + tableMembers + " WHERE region=?;", region.getName());
        try {
            while (result != null && result.next()) {
                members.put(UUID.fromString(result.getString("uuid")), RegionRank.valueOf(result.getString("rank")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return members;
    }

    private List<Flag> getFlags(Region region) {
        List<Flag> flags = new ArrayList<Flag>();

        ResultSet result = query("SELECT flag FROM " + tableFlags + " WHERE region=?;", region.getName());
        try {
            while (result != null && result.next()) {
                flags.add(Flag.valueOf(result.getString("flag")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return flags;
    }

    private List<UUID> getBannedPlayers(Region region) {
        List<UUID> bannedPlayers = new ArrayList<UUID>();

        ResultSet result = query("SELECT uuid FROM " + tableBannedPlayers + " WHERE region=?;", region.getName());
        try {
            while (result != null && result.next()) {
                bannedPlayers.add(UUID.fromString(result.getString("uuid")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return bannedPlayers;
    }

    public void unload() {
        try {
            connection.close();
            connection = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
