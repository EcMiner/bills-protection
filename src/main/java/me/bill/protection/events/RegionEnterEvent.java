package me.bill.protection.events;

import me.bill.protection.region.Region;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class RegionEnterEvent extends PlayerEvent implements Cancellable {

    public static final HandlerList handlers = new HandlerList();

    public static HandlerList getHandlerList() {
        return handlers;
    }

    private boolean cancelled;
    private final Region fromRegion;
    private final Region toRegion;
    private final Location from;
    private final Location to;

    public RegionEnterEvent(Player player, Region fromRegion, Region toRegion, Location from, Location to) {
        super(player);
        this.fromRegion = fromRegion;
        this.toRegion = toRegion;
        this.from = from;
        this.to = to;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Region getFromRegion() {
        return fromRegion;
    }

    public Region getToRegion() {
        return toRegion;
    }

    public Location getFrom() {
        return from;
    }

    public Location getTo() {
        return to;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
