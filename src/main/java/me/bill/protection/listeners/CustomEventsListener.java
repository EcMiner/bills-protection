package me.bill.protection.listeners;

import me.bill.protection.Protection;
import me.bill.protection.events.RegionEnterEvent;
import me.bill.protection.region.Region;
import me.bill.protection.region.RegionManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

public class CustomEventsListener implements Listener {

    private final RegionManager regionManager = Protection.getInstance().getRegionManager();

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent evt) {
        Player player = evt.getPlayer();

        if (evt.getFrom().getX() != evt.getTo().getX() || evt.getFrom().getY() != evt.getTo().getY() || evt.getFrom().getZ() != evt.getTo().getZ()) {
            Region from = regionManager.getInRegion(evt.getFrom());
            Region to = regionManager.getInRegion(evt.getTo());

            //System.out.println("From: " + (from != null ? from.getName() : "null"));
            //System.out.println("To: " + (to != null ? to.getName() : "null"));

            if ((from == null && to != null) || (from != to)) {
                //System.out.println("CALLING");
                RegionEnterEvent event = new RegionEnterEvent(player, from, to, evt.getFrom(), evt.getTo());
                Protection.getInstance().getServer().getPluginManager().callEvent(event);
                evt.setCancelled(event.isCancelled());
            }
        }
    }

    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent evt) {
        Player player = evt.getPlayer();
        Region from = regionManager.getInRegion(evt.getFrom());
        Region to = regionManager.getInRegion(evt.getTo());

        if ((from == null && to != null) || (from != to)) {
            RegionEnterEvent event = new RegionEnterEvent(player, from, to, evt.getFrom(), evt.getTo());
            Protection.getInstance().getServer().getPluginManager().callEvent(event);
            evt.setCancelled(event.isCancelled());
        }
    }

}
