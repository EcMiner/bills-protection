package me.bill.protection.listeners;

import me.bill.protection.Protection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerLogin(AsyncPlayerPreLoginEvent evt) {
        if (evt.getLoginResult() == AsyncPlayerPreLoginEvent.Result.ALLOWED) {
            Protection.getInstance().getPlayerManager().loadData(evt.getUniqueId());
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent evt) {
        Player player = evt.getPlayer();
        Protection.getInstance().getPlayerManager().removePlayerData(player);
    }

}
