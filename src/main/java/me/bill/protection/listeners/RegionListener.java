package me.bill.protection.listeners;

import me.bill.protection.Protection;
import me.bill.protection.chat.Chat;
import me.bill.protection.chat.ChatType;
import me.bill.protection.events.RegionEnterEvent;
import me.bill.protection.permission.Permissions;
import me.bill.protection.player.PlayerData;
import me.bill.protection.region.Flag;
import me.bill.protection.region.Region;
import me.bill.protection.region.RegionManager;
import me.bill.protection.region.SafeLocation;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.*;

public class RegionListener implements Listener {

    private final RegionManager regionManager = Protection.getInstance().getRegionManager();

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent evt) {
        if ((evt.getAction() == Action.RIGHT_CLICK_BLOCK || evt.getAction() == Action.LEFT_CLICK_BLOCK) && evt.getItem() != null) {
            Player player = evt.getPlayer();
            PlayerData playerData = Protection.getInstance().getPlayerManager().getPlayerData(player);
            if (playerData.isInSelectionMode()) {
                Material selectionTool = playerData.getSelectionTool() != null ? playerData.getSelectionTool() : Protection.getInstance().getConfiguration().getSelectionTool();

                if (evt.getItem().getType() == selectionTool && player.hasPermission(Permissions.selectionSelect)) {
                    evt.setCancelled(true);
                    Block block = evt.getClickedBlock();

                    Region inRegion = regionManager.getInRegion(block.getLocation());
                    if (inRegion == null) {
                        if (evt.getAction() == Action.RIGHT_CLICK_BLOCK) {
                            playerData.setSelectedLoc1(new SafeLocation(block));
                        } else {
                            playerData.setSelectedLoc2(new SafeLocation(block));
                        }
                        Chat.sendMessage(player, ChatType.SUCCESS, (evt.getAction() == Action.RIGHT_CLICK_BLOCK ? "Second" : "First") +
                                " location selected at {%m}" + block.getX() + "{%n}, {%m}" + block.getY() + "{%n}, {%m}" + block.getZ());
                    } else {
                        Chat.sendMessage(player, ChatType.ERROR, "A region already exists at this location! (region: {%m}" + inRegion.getName() + "{%n})");
                    }
                } else if (evt.getItem().getType() == selectionTool) {
                    Chat.noPerm(player, Permissions.selectionSelect);
                }
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent evt) {
        Player player = evt.getPlayer();
        if (notAllowed(Flag.NO_BREAK, player, evt.getBlock().getLocation())) {
            evt.setCancelled(true);
            Chat.sendMessage(player, ChatType.ERROR, "You can't break blocks in this region!");
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent evt) {
        Player player = evt.getPlayer();
        if (notAllowed(Flag.NO_PLACE, player, evt.getBlock().getLocation())) {
            evt.setCancelled(true);
            Chat.sendMessage(player, ChatType.ERROR, "You can't place blocks in this region!");
        }
    }

    @EventHandler
    public void onDropItem(PlayerDropItemEvent evt) {
        Player player = evt.getPlayer();
        if (notAllowed(Flag.NO_ITEM_DROP, player)) {
            evt.setCancelled(true);
            Chat.sendMessage(player, ChatType.ERROR, "You can't drop items in this region!");
        }
    }

    @EventHandler
    public void onPickupItem(PlayerPickupItemEvent evt) {
        Player player = evt.getPlayer();
        if (notAllowed(Flag.NO_ITEM_PICKUP, player)) {
            evt.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntitySpawn(CreatureSpawnEvent evt) {
        evt.setCancelled(notAllowed(Flag.NO_MOB_SPAWN, evt.getLocation()) ||
                (notAllowed(Flag.NO_MONSTER_SPAWN, evt.getLocation()) && evt.getEntity() instanceof Monster) ||
                (notAllowed(Flag.NO_ANIMAL_SPAWN, evt.getLocation()) && evt.getEntity() instanceof Animals));
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent evt) {
        if (evt.getEntity() instanceof Player && evt.getDamager() instanceof Player) {
            Player player = (Player) evt.getEntity();
            Player damager = (Player) evt.getDamager();
            if (notAllowed(Flag.NO_PVP, damager) || notAllowed(Flag.NO_PVP, damager, player.getLocation())) {
                Chat.sendMessage(player, ChatType.ERROR, "You can't PvP in this region");
                evt.setCancelled(true);
            }
        } else if (evt.getEntity() instanceof Player && evt.getDamager() instanceof Projectile) {
            Player player = (Player) evt.getEntity();
            Projectile projectile = (Projectile) evt.getDamager();
            if (projectile.getShooter() != null && projectile.getShooter() instanceof Player) {
                Player shooter = (Player) projectile.getShooter();
                if (notAllowed(Flag.NO_PVP, shooter) || notAllowed(Flag.NO_PVP, shooter, player.getLocation())) {
                    Chat.sendMessage(player, ChatType.ERROR, "You can't PvP in this region");
                    evt.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent evt) {
        if ((evt.getEntityType() == EntityType.PRIMED_TNT && notAllowed(Flag.NO_TNT_EXPLOSION, evt.getEntity().getLocation())) ||
                (evt.getEntityType() == EntityType.CREEPER && notAllowed(Flag.NO_CREEPER_EXPLOSION, evt.getEntity().getLocation())) ||
                notAllowed(Flag.NO_EXPLOSIONS, evt.getEntity().getLocation())) {
            evt.setCancelled(true);
        }
    }

    @EventHandler
    public void onBucketFill(PlayerBucketFillEvent evt) {
        if (notAllowed(Flag.NO_BUCKET_FILL, evt.getPlayer(), evt.getBlockClicked().getLocation())) {
            evt.setCancelled(true);
            Chat.sendMessage(evt.getPlayer(), ChatType.ERROR, "You can't fill your bucket in this region!");
        }
    }

    @EventHandler
    public void onBucketEmpty(PlayerBucketEmptyEvent evt) {
        if (notAllowed(Flag.NO_BUCKET_EMPTY, evt.getPlayer(), evt.getBlockClicked().getLocation())) {
            evt.setCancelled(true);
            Chat.sendMessage(evt.getPlayer(), ChatType.ERROR, "You can't empty your bucket in this region!");
        }
    }

    @EventHandler
    public void onBlockIgnite(BlockIgniteEvent evt) {
        if (evt.getCause() == BlockIgniteEvent.IgniteCause.SPREAD && notAllowed(Flag.NO_FIRE_SPREAD, evt.getIgnitingBlock().getLocation())) {
            evt.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockSpread(BlockSpreadEvent evt) {
        if (evt.getNewState().getType() == Material.FIRE && notAllowed(Flag.NO_FIRE_SPREAD, evt.getNewState().getLocation())) {
            evt.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockFromTo(BlockFromToEvent evt) {
        if (((evt.getToBlock().getType() == Material.WATER || evt.getToBlock().getType() == Material.STATIONARY_WATER) ||
                (evt.getToBlock().getType() != Material.WATER && evt.getToBlock().getType() != Material.STATIONARY_WATER) &&
                        (evt.getBlock().getType() == Material.WATER || evt.getBlock().getType() == Material.STATIONARY_WATER)) &&
                notAllowed(Flag.NO_WATER_SPREAD, evt.getToBlock().getLocation())) {
            evt.setCancelled(true);
        } else if (((evt.getToBlock().getType() == Material.LAVA || evt.getToBlock().getType() == Material.STATIONARY_LAVA) ||
                (evt.getToBlock().getType() != Material.LAVA && evt.getToBlock().getType() != Material.STATIONARY_LAVA) &&
                        (evt.getBlock().getType() == Material.LAVA || evt.getBlock().getType() == Material.STATIONARY_LAVA)) &&
                notAllowed(Flag.NO_LAVA_SPREAD, evt.getToBlock().getLocation())) {
            evt.setCancelled(true);
        } else if ((evt.getBlock().getType() == Material.SNOW_BLOCK || evt.getBlock().getType() == Material.SNOW || evt.getBlock().getType() == Material.ICE
                || evt.getBlock().getType() == Material.PACKED_ICE) && (evt.getToBlock().getType() == Material.WATER || evt.getToBlock().getType() == Material.STATIONARY_WATER)
                && notAllowed(Flag.NO_BLOCKS_MELT, evt.getToBlock().getLocation())) {
            evt.setCancelled(true);
        }
    }

    @EventHandler
    public void onRegionEnter(RegionEnterEvent evt) {
        Player player = evt.getPlayer();
        if (evt.getToRegion() != null && (evt.getToRegion().hasFlag(Flag.NO_ENTRY) && !(evt.getToRegion().isOwner(player) || evt.getToRegion().isMember(player)))) {
            evt.setCancelled(true);
            Chat.sendMessage(player, ChatType.ERROR, "You can't enter this region!");
        }
    }

    @EventHandler
    public void onBlockPhysics(BlockPhysicsEvent evt) {
        if (notAllowed(Flag.NO_PHYSICS, evt.getBlock().getLocation())) {
            evt.setCancelled(true);
        }
    }

    @EventHandler
    public void onLeavesDecay(LeavesDecayEvent evt) {
        if (notAllowed(Flag.NO_LEAVES_DECAY, evt.getBlock().getLocation())) {
            evt.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockRedstone(BlockRedstoneEvent evt) {
        if (notAllowed(Flag.NO_REDSTONE, evt.getBlock().getLocation())) {
            evt.setNewCurrent(0);
        }
    }

    @EventHandler
    public void onPistonExtended(BlockPistonExtendEvent evt) {
        if (notAllowed(Flag.NO_REDSTONE, evt.getBlock().getLocation())) {
            evt.setCancelled(true);
        }
    }

    @EventHandler
    public void onHangingBreak(HangingBreakByEntityEvent evt) {
        if (evt.getRemover() instanceof Player) {
            Player player = (Player) evt.getRemover();
            if (notAllowed(Flag.NO_BREAK, player, evt.getEntity().getLocation())) {
                evt.setCancelled(true);
                Chat.sendMessage(player, ChatType.ERROR, "You can't break blocks in this region!");
            }
        }
    }

    @EventHandler
    public void onHangingPlace(HangingPlaceEvent evt) {
        Player player = evt.getPlayer();
        if (notAllowed(Flag.NO_PLACE, player, evt.getEntity().getLocation())) {
            evt.setCancelled(true);
            Chat.sendMessage(player, ChatType.ERROR, "You can't place blocks in this region!");
        }
    }

    private boolean notAllowed(Flag flag, Player player) {
        return notAllowed(flag, player, player.getLocation());
    }

    private boolean notAllowed(Flag flag, Player player, Location loc) {
        Region region = regionManager.getInRegion(player);
        return region != null && region.hasFlag(flag) && (!region.isOwner(player) && !region.isMember(player));
    }

    private boolean notAllowed(Flag flag, Location loc) {
        Region region = regionManager.getInRegion(loc);
        return region != null && region.hasFlag(flag);
    }

}
