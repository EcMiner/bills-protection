package me.bill.protection.logging;

import java.io.*;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;

public class FileLogger {

    static ExecutorService loggerService = Executors.newSingleThreadExecutor();

    private final File file;

    public FileLogger(String loggerName, File file) {
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public void log(final Level level, final String message) {
        loggerService.submit(new Runnable() {

            @Override
            public void run() {
                Calendar cal = new Calendar.Builder().setInstant(new Date(System.currentTimeMillis())).build();
                String msg = "[" + cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.WEEK_OF_YEAR) + "/" + cal.get(Calendar.YEAR)
                        + " " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND) + " " + level.getName() + "] " +
                        message;
                try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)))) {
                    out.println(msg);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

        });
    }
}
