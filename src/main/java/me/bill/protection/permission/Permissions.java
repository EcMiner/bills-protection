package me.bill.protection.permission;

public class Permissions {

    public static final String regionHelp = "protection.region.help";
    public static final String createRegion = "protection.region.create";
    public static final String deleteRegion = "protection.region.delete";
    public static final String deleteAnyRegion = "protection.region.delete.any";
    public static final String regionBanPlayer = "protection.region.ban";
    public static final String regionUnbanPlayer = "protection.region.unban";
    public static final String renameRegion = "protection.region.rename";
    public static final String addFlag = "protection.region.flag.add";
    public static final String removeFlag = "protection.region.flag.remove";
    public static final String addMember = "protection.region.member.add";
    public static final String kickMember = "protection.region.member.kick";
    public static final String setRank = "protection.region.member.setrank";
    public static final String regionInfo = "protection.region.info";

    public static final String selectionHelp = "protection.selection.help";
    public static final String selectionToggle = "protection.selection.toggle";
    public static final String selectionToolView = "protection.selection.tool.view";
    public static final String selectionToolSet = "protection.selection.tool.set";
    public static final String selectionSelect = "protection.selection.select";

    public static final String flagsHelp = "protection.flag.help";

    public static final String configHelp = "protection.config.help";
    public static final String configReload = "protection.config.reload";
    public static final String configToolView = "protection.config.tool.view";
    public static final String configToolSet = "protection.config.tool.set";
    public static final String configAdminsCanBanView = "protection.config.adminscanban.view";
    public static final String configAdminsCanBanSet = "protection.config.adminscanban.set";
    public static final String configAdminsCanUnbanView = "protection.config.adminscanunban.view";
    public static final String configAdminsCanUnbanSet = "protection.config.adminscanunban.set";

}
