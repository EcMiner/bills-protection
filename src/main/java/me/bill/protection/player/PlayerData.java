package me.bill.protection.player;

import me.bill.protection.region.SafeLocation;
import me.bill.protection.synchronization.Async;
import me.bill.protection.Protection;
import me.bill.protection.database.Database;
import org.bukkit.Material;

import java.util.UUID;

public class PlayerData {

    private final UUID playerId;
    private boolean existsInDatabase;
    private Material selectionTool;

    private SafeLocation selectedLoc1;
    private SafeLocation selectedLoc2;
    private boolean selectionMode;

    public PlayerData(UUID playerId, boolean existsInDatabase) {
        this.playerId = playerId;
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public Material getSelectionTool() {
        return selectionTool;
    }

    public void setSelectionTool(Material selectionTool) {
        setSelectionTool(selectionTool, true);
    }

    public void setSelectionTool(final Material selectionTool, boolean updateDB) {
        this.selectionTool = selectionTool;
        if (updateDB) {
            Async.run(new Runnable() {

                @Override
                public void run() {
                    Database database = Protection.getInstance().getDataBase();
                    if (existsInDatabase) {
                        database.setPlayerSelectionTool(playerId, selectionTool);
                    } else {
                        database.createPlayerData(playerId, selectionTool);
                        existsInDatabase = true;
                    }
                }

            });
        }
    }

    public SafeLocation getSelectedLoc1() {
        return selectedLoc1;
    }

    public SafeLocation getSelectedLoc2() {
        return selectedLoc2;
    }

    public void setSelectedLoc1(SafeLocation selectedLoc1) {
        this.selectedLoc1 = selectedLoc1;
    }

    public void setSelectedLoc2(SafeLocation selectedLoc2) {
        this.selectedLoc2 = selectedLoc2;
    }

    public boolean isInSelectionMode() {
        return selectionMode;
    }

    public void setInSelectionMode(boolean selectionMode) {
        this.selectionMode = selectionMode;
    }
}
