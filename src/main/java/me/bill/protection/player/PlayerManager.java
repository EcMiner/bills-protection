package me.bill.protection.player;

import me.bill.protection.Protection;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerManager {

    private Map<UUID, PlayerData> storedData = new HashMap<UUID, PlayerData>();

    public PlayerData loadData(UUID playerId) {
        PlayerData playerData = Protection.getInstance().getDataBase().loadPlayerData(playerId);
        if (playerData == null) {
            playerData = new PlayerData(playerId, false);
        }
        storedData.put(playerId, playerData);
        return playerData;
    }

    public PlayerData getPlayerData(Player player) {
        return storedData.get(player.getUniqueId());
    }

    public void removePlayerData(Player player) {
        storedData.remove(player.getUniqueId());
    }

    public void unload() {
        storedData = null;
    }

}
