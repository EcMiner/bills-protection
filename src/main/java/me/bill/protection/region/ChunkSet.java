package me.bill.protection.region;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bukkit.Chunk;

public class ChunkSet {

    private final int x;
    private final int z;

    public ChunkSet(Chunk chunk) {
        this(chunk.getX(), chunk.getZ());
    }

    public ChunkSet(int x, int z) {
        this.x = x;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public int getZ() {
        return z;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        ChunkSet set = (ChunkSet) obj;
        return new EqualsBuilder().append(x, set.x).append(z, set.z).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31).append(x).append(z).toHashCode();
    }
}
