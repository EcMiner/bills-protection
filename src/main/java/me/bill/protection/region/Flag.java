package me.bill.protection.region;

public enum Flag {

    NO_BREAK("Prevents players that aren't members of the region from breaking blocks"),
    NO_PLACE("Prevents players that aren't members of the region from placing blocks"),
    NO_ITEM_DROP("Prevents players that aren't members of the region from dropping items"),
    NO_ITEM_PICKUP("Prevents players that aren't members of the region from picking up items"),
    NO_MOB_SPAWN("Prevents any kind of mobs from spawning in the region"),
    NO_MONSTER_SPAWN("Prevents monsters (i.e. zombies, spiders, etc.) from spawning in the region"),
    NO_ANIMAL_SPAWN("Prevents animals (i.e. sheep, wolfs, etc.) from spawning in the region"),
    NO_PVP("Prevents players that aren't members of the region from pvp-ing in the region"),
    NO_DEATH_DROPS("Prevents dropped items from spawning, only for players that aren't a member of the region"),
    NO_TNT_EXPLOSION("Prevents tnt from exploding in the region"),
    NO_CREEPER_EXPLOSION("Prevents creepers from exploding in the region"),
    NO_EXPLOSIONS("Prevents any kind of explosion in the region"),
    NO_BUCKET_EMPTY("Prevents players that aren't members of the region from emptying lava/water buckets"),
    NO_BUCKET_FILL("Prevents players that aren't members of the region from filling their buckets with water/lava"),
    NO_FIRE_SPREAD("Prevents any fire from spreading in the region"),
    NO_WATER_SPREAD("Prevents any water from spreading in the region"),
    NO_LAVA_SPREAD("Prevents any lava from spreading in the region"),
    NO_BLOCKS_MELT("Prevents any blocks from melting, like ice and snow"),
    NO_ENTRY("Prevents player that aren't members of the region from entering the region"),
    NO_PHYSICS("Prevents any form of block physics in the region"),
    NO_LEAVES_DECAY("Prevents leaves from decaying when a tree is chopped down"),
    NO_REDSTONE("Prevents any form or redstone mechanisms in the region");

    public static Flag fromName(String flagName) {
        for (Flag flag : values()) {
            if (flag.name().replace("_", "").equalsIgnoreCase(flagName.replace("_", "").replace("-", ""))) {
                return flag;
            }
        }
        return null;
    }

    private final String description;

    Flag(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
