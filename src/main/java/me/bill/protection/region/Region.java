package me.bill.protection.region;

import me.bill.protection.Protection;
import me.bill.protection.logging.FileLogger;
import me.bill.protection.synchronization.Async;
import org.apache.commons.io.FileUtils;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Region {

    private final UUID owner;
    private FileLogger logger;
    private String name;
    private SafeLocation max;
    private SafeLocation min;
    private List<Flag> flags = new ArrayList<Flag>();
    private Map<UUID, RegionRank> members = new HashMap<UUID, RegionRank>();
    private List<UUID> bannedPlayers = new ArrayList<>();

    public Region(String name, UUID owner, SafeLocation loc1, SafeLocation loc2) {
        this.name = name;
        this.owner = owner;
        this.max = SafeLocation.max(loc1, loc2);
        this.min = SafeLocation.min(loc1, loc2);
        this.logger = new FileLogger(name, new File(Protection.getInstance().getDataFolder() + File.separator + "logs" + File.separator + "regions" + File.separator + name + ".txt"));
    }

    public String getName() {
        return name;
    }

    public UUID getOwner() {
        return owner;
    }

    public FileLogger getLogger() {
        return logger;
    }

    public boolean isOwner(Player player) {
        return isOwner(player.getUniqueId());
    }

    public boolean isOwner(UUID playerId) {
        return owner.equals(playerId);
    }

    public SafeLocation getMax() {
        return max;
    }

    public SafeLocation getMin() {
        return min;
    }

    public List<Flag> getFlags() {
        return flags;
    }

    public boolean hasFlag(Flag flag) {
        return flags.contains(flag);
    }

    public void addFlag(Flag flag) {
        addFlag(flag, true);
    }

    public void addFlag(final Flag flag, boolean updateDB) {
        if (!flags.contains(flag)) {
            flags.add(flag);
            if (updateDB) {
                Async.run(new Runnable() {

                    @Override
                    public void run() {
                        Protection.getInstance().getDataBase().addFlag(Region.this, flag);
                    }

                });
            }
        }
    }

    public void removeFlag(Flag flag) {
        removeFlag(flag, true);
    }

    public void removeFlag(final Flag flag, boolean updateDB) {
        if (flags.contains(flag)) {
            flags.remove(flag);
            if (updateDB) {
                Async.run(new Runnable() {

                    @Override
                    public void run() {
                        Protection.getInstance().getDataBase().removeFlag(Region.this, flag);
                    }

                });
            }
        }
    }

    public Map<UUID, RegionRank> getMembers() {
        return members;
    }

    public void addMember(UUID playerId) {
        addMember(playerId, true);
    }

    public void addMember(final UUID playerId, boolean updateDB) {
        if (!members.containsKey(playerId)) {
            members.put(playerId, RegionRank.MEMBER);
            if (updateDB) {
                Async.run(new Runnable() {

                    @Override
                    public void run() {
                        Protection.getInstance().getDataBase().addMember(Region.this, playerId);
                    }

                });
            }
        }
    }

    public void removeMember(UUID playerId) {
        removeMember(playerId, true);
    }

    public void removeMember(final UUID playerId, boolean updateDB) {
        if (members.containsKey(playerId)) {
            members.remove(playerId);
            if (updateDB) {
                Async.run(new Runnable() {

                    @Override
                    public void run() {
                        Protection.getInstance().getDataBase().removeMember(Region.this, playerId);
                    }

                });
            }
        }
    }

    public boolean isMember(Player player) {
        return isMember(player.getUniqueId());
    }

    public boolean isMember(UUID playerId) {
        return members.containsKey(playerId);
    }

    public RegionRank getMemberRank(Player player) {
        return getMemberRank(player.getUniqueId());
    }

    public RegionRank getMemberRank(UUID playerId) {
        return members.get(playerId);
    }

    public void setRank(UUID playerId, RegionRank rank) {
        setRank(playerId, rank, true);
    }

    public void setRank(final UUID playerId, final RegionRank rank, boolean updateDB) {
        if (members.containsKey(playerId) && members.get(playerId) != rank) {
            members.put(playerId, rank);
            if (updateDB) {
                Async.run(new Runnable() {

                    @Override
                    public void run() {
                        Protection.getInstance().getDataBase().setMemberRank(Region.this, playerId, rank);
                    }

                });
            }
        }
    }

    public List<UUID> getBannedPlayers() {
        return bannedPlayers;
    }

    public boolean isBanned(UUID playerId) {
        return bannedPlayers.contains(playerId);
    }

    public void banPlayer(UUID playerId) {
        banPlayer(playerId, true);
    }

    public void banPlayer(final UUID playerId, boolean updateDB) {
        if (!bannedPlayers.contains(playerId)) {
            bannedPlayers.add(playerId);
            if (updateDB) {
                Async.run(new Runnable() {
                    @Override
                    public void run() {
                        Protection.getInstance().getDataBase().banPlayer(Region.this, playerId);
                    }
                });
            }
        }
    }

    public void unBanPlayer(UUID playerId) {
        unBanPlayer(playerId, true);
    }

    public void unBanPlayer(final UUID playerId, boolean updateDB) {
        if (bannedPlayers.contains(playerId)) {
            bannedPlayers.remove(playerId);
            if (updateDB) {
                Async.run(new Runnable() {
                    @Override
                    public void run() {
                        Protection.getInstance().getDataBase().unBanPlayer(Region.this, playerId);
                    }
                });
            }
        }
    }

    protected void setName(String name) {
        this.name = name;
        File newFile = new File(Protection.getInstance().getDataFolder() + File.separator + "logs" + File.separator + "regions" + File.separator + name + ".txt");

        try {
            FileUtils.copyFile(logger.getFile(), newFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.getFile().delete();
        logger = new FileLogger(name, newFile);
    }

    public boolean isInRegion(Entity ent) {
        return isInRegion(ent.getLocation());
    }

    public boolean isInRegion(SafeLocation loc) {
        return isInRegion(loc.getLocation());
    }

    public boolean isInRegion(Location loc) {
        return loc.getX() <= max.getX() && loc.getX() >= min.getX() &&
                loc.getY() <= max.getY() && loc.getY() >= min.getY() &&
                loc.getZ() <= max.getZ() && loc.getZ() >= min.getZ();
    }

    public boolean intersects(Region region) {
        return intersects(region.getMax(), region.getMin());
    }

    public boolean intersects(SafeLocation loc1, SafeLocation loc2) {
        return intersects(loc1.getLocation(), loc2.getLocation());
    }

    public boolean intersects(Location loc1, Location loc2) {
        SafeLocation max = SafeLocation.max(loc1, loc2);
        SafeLocation min = SafeLocation.min(loc1, loc2);

        return this.min.getX() <= max.getX() && this.max.getX() >= min.getX() &&
                this.min.getY() <= max.getY() && this.max.getY() >= min.getY() &&
                this.min.getZ() <= max.getZ() && this.max.getZ() >= min.getZ();
    }
}
