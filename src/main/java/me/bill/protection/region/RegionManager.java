package me.bill.protection.region;

import me.bill.protection.Protection;
import me.bill.protection.logging.FileLogger;
import me.bill.protection.synchronization.Async;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegionManager {

    private Map<String, Region> regions = new HashMap<String, Region>();
    private Map<ChunkSet, List<Region>> regionChunkLookup = new HashMap<ChunkSet, List<Region>>();
    private final FileLogger logger;

    public RegionManager() {
        logger = new FileLogger("Region Logger", new File(Protection.getInstance().getDataFolder() + File.separator + "logs" +
                File.separator + "region_log.txt"));
        loadRegions();
    }

    public FileLogger getLogger() {
        return logger;
    }

    private void loadRegions() {
        for (Region region : Protection.getInstance().getDataBase().loadAllRegions()) {
            regions.put(region.getName().toLowerCase(), region);

            for (ChunkSet chunk : getOverlappingChunks(region)) {
                addToChunk(chunk, region);
            }
        }
    }

    private void addToChunk(ChunkSet chunk, Region region) {
        if (!regionChunkLookup.containsKey(chunk)) {
            regionChunkLookup.put(chunk, new ArrayList<Region>());
        }
        regionChunkLookup.get(chunk).add(region);
    }

    public List<Region> getRegionsInChunk(Chunk chunk) {
        ChunkSet set = new ChunkSet(chunk);
        return getRegionsInChunk(set);
    }

    public List<Region> getRegionsInChunk(ChunkSet chunk) {
        return regionChunkLookup.containsKey(chunk) ? regionChunkLookup.get(chunk) : new ArrayList<Region>();
    }

    public boolean isRegion(String name) {
        return regions.containsKey(name.toLowerCase());
    }

    public Region getRegion(String name) {
        return regions.get(name.toLowerCase());
    }

    public Region createRegion(String name, Player player, SafeLocation loc1, SafeLocation loc2) {
        if (!isRegion(name)) {
            final Region region = new Region(name, player.getUniqueId(), loc1, loc2);
            regions.put(name.toLowerCase(), region);

            for (ChunkSet chunk : getOverlappingChunks(region)) {
                addToChunk(chunk, region);
            }

            Async.run(new Runnable() {

                @Override
                public void run() {
                    Protection.getInstance().getDataBase().createRegion(region);
                }

            });
            return region;
        }
        return null;
    }

    public void deleteRegion(final Region region) {
        regions.remove(region.getName().toLowerCase());
        for (ChunkSet chunk : getOverlappingChunks(region)) {
            if (regionChunkLookup.containsKey(chunk)) {
                regionChunkLookup.get(chunk).remove(region);
                if (regionChunkLookup.get(chunk).size() == 0) {
                    regionChunkLookup.remove(chunk);
                }
            }
        }
        Async.run(new Runnable() {
            @Override
            public void run() {
                Protection.getInstance().getDataBase().deleteRegion(region);
            }
        });
    }

    public List<ChunkSet> getOverlappingChunks(Region region) {
        return getOverlappingChunks(region.getMax(), region.getMin());
    }

    public List<ChunkSet> getOverlappingChunks(SafeLocation loc1, SafeLocation loc2) {
        return getOverlappingChunks(loc1.getLocation(), loc2.getLocation());
    }

    public List<ChunkSet> getOverlappingChunks(Location loc1, Location loc2) {
        List<ChunkSet> chunks = new ArrayList<ChunkSet>();
        int maxChunkX = Math.max(loc1.getChunk().getX(), loc2.getChunk().getX());
        int minChunkX = Math.min(loc1.getChunk().getX(), loc2.getChunk().getX());
        int maxChunkZ = Math.max(loc1.getChunk().getZ(), loc2.getChunk().getZ());
        int minChunkZ = Math.min(loc1.getChunk().getZ(), loc2.getChunk().getZ());

        for (int chunkX = minChunkX; chunkX <= maxChunkX; chunkX++) {
            for (int chunkZ = minChunkZ; chunkZ <= maxChunkZ; chunkZ++) {
                chunks.add(new ChunkSet(chunkX, chunkZ));
            }
        }
        return chunks;
    }

    public void renameRegion(Region region, String name) {
        renameRegion(region, name, true);
    }

    public void renameRegion(final Region region, final String name, boolean updateDB) {
        final String oldName = region.getName();
        if (updateDB) {
            Async.run(new Runnable() {

                @Override
                public void run() {
                    Protection.getInstance().getDataBase().renameRegion(oldName, name);
                }

            });
        }
        regions.remove(region.getName().toLowerCase());
        regions.put(name.toLowerCase(), region);
        region.setName(name);
    }

    public Region getInRegion(Entity entity) {
        return getInRegion(entity.getLocation());
    }

    public Region getInRegion(Location loc) {
        for (Region region : getRegionsInChunk(loc.getChunk())) {
            if (region.isInRegion(loc)) {
                return region;
            }
        }
        return null;
    }

    public void unload() {
        regions = null;
        regionChunkLookup = null;
    }

}
