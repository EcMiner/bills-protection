package me.bill.protection.region;

public enum RegionRank {

    MEMBER(9), ADMIN(10);

    private final int rankLadder;

    RegionRank(int rankLadder) {
        this.rankLadder = rankLadder;
    }

    public static RegionRank fromName(String name) {
        for (RegionRank rank : values()) {
            if (rank.name().equalsIgnoreCase(name)) {
                return rank;
            }
        }
        return null;
    }

    public int getRankLadder() {
        return rankLadder;
    }

    public boolean isHigherOrEqualsThan(RegionRank rank) {
        return this.rankLadder >= rank.rankLadder;
    }

    public boolean isHigherThan(RegionRank rank) {
        return this.rankLadder > rank.rankLadder;
    }

    public boolean isLowerOrEqualsThan(RegionRank rank) {
        return this.rankLadder <= rank.rankLadder;
    }

    public boolean isLowerThan(RegionRank rank) {
        return this.rankLadder < rank.rankLadder;
    }
}
