package me.bill.protection.region;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;

public class SafeLocation {

    public static SafeLocation fromString(String safeLocation) {
        try {
            String[] split = safeLocation.split(",");
            return new SafeLocation(split[0], Double.parseDouble(split[1]), Double.parseDouble(split[2]),
                    Double.parseDouble(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[5]));
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("The given string has the wrong format");
        } catch (ArrayIndexOutOfBoundsException ex1) {
            throw new IllegalArgumentException("The given string has the wrong format");
        }
    }

    public static SafeLocation max(SafeLocation loc1, SafeLocation loc2) {
        return max(loc1.getLocation(), loc2.getLocation());
    }

    public static SafeLocation max(Location loc1, Location loc2) {
        return new SafeLocation(loc1.getWorld().getName(), Math.max(loc1.getX(), loc2.getX()), Math.max(loc1.getY(), loc2.getY()),
                Math.max(loc1.getZ(), loc2.getZ()));
    }

    public static SafeLocation min(SafeLocation loc1, SafeLocation loc2) {
        return min(loc1.getLocation(), loc2.getLocation());
    }

    public static SafeLocation min(Location loc1, Location loc2) {
        return new SafeLocation(loc1.getWorld().getName(), Math.min(loc1.getX(), loc2.getX()), Math.min(loc1.getY(), loc2.getY()),
                Math.min(loc1.getZ(), loc2.getZ()));
    }

    private double x, y, z;
    private float yaw, pitch;
    private String world;

    public SafeLocation(Block block) {
        this(block.getLocation());
    }

    public SafeLocation(Location loc) {
        this.x = loc.getX();
        this.y = loc.getY();
        this.z = loc.getZ();
        this.yaw = loc.getYaw();
        this.pitch = loc.getPitch();
        this.world = loc.getWorld().getName();
    }

    public SafeLocation(String world) {
        this.world = world;
    }

    public SafeLocation(String world, double x, double y, double z) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public SafeLocation(String world, double x, double y, double z, float yaw, float pitch) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public float getYaw() {
        return yaw;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    public String getWorld() {
        return world;
    }

    public SafeLocation setWorld(String world) {
        this.world = world;
        return this;
    }

    public Location getLocation() {
        return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    }

    public Block getBlock() {
        return getLocation().getBlock();
    }

    @Override
    public String toString() {
        return world + "," + x + "," + y + "," + z + "," + yaw + "," + pitch;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass() == getClass() && this.toString().equals(obj.toString());
    }

}