package me.bill.protection.synchronization;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Async {

    private static final ExecutorService threadPool = Executors.newFixedThreadPool(5);

    public static <T> T get(Callable<T> callable) {
        try {
            return threadPool.submit(callable).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void run(Runnable runnable) {
        threadPool.submit(runnable);
    }

    public static void unload() {
        threadPool.shutdownNow();
    }

}
